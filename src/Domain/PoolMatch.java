package Domain;

/**
 *
 * @author Nour
 */
public class PoolMatch extends Match {
//    private int getPoolName;

    private String poolName;

  public PoolMatch(Team team1, Team team2, String poolName){
      addTeam(team1);
      addTeam(team2);
      this.poolName = poolName;
  }

    

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolID) {
        this.poolName = poolID;
    }

    
}
