package Domain;

/**
 *
 * @author Nour
 */
public class TournamentException extends Exception
{   
    public TournamentException(String message)
    {
        super(message);
    }
}
