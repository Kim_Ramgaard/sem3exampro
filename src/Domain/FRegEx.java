package Domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Nour
 */
public class FRegEx {

    private String name;
    private String email;
    private String phoneNo;

    //Testing purposes
    public FRegEx() {

    }

    public FRegEx(String name, String email, String phoneNo) {
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
    }

    //accessors and mutators
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /*
     ** emailValidation method checks if the email address entered by the customer
     ** has the criterias of an email address
     */
    public static void emailValidation(String email) throws InvalidCheckException {
        Pattern pattern = Pattern.compile("(\\w{2,})@([a-z1-9]{2,})[.][a-z]{2,3}");
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            throw new InvalidCheckException("This email is invalid! :(");
        }
    }

    /*
     ** phoneNoValidation method checks if the phone number entered by the customer
     ** if it is only numbers and it accepts to begin with + sign for the code of
     ** each country
     */
    public static void phoneNoValidation(String phoneNo) throws InvalidCheckException {
        Pattern pattern = Pattern.compile("([+]?)[0-9]{8,19}");
        Matcher matcher = pattern.matcher(phoneNo);
        if (!matcher.matches()) {
            throw new InvalidCheckException("This Phone no. is invalid! :(");
        }
    }

    /*
     ** nameValidation method checks the name if it is not null and more starts 
     ** with an uppercase letter
     */
    public static void nameValidation(String name) throws InvalidCheckException {
        Pattern pattern = Pattern.compile("[A-Z]([a-z]{0,})");
        Matcher matcher = pattern.matcher(name);
        if (!matcher.matches()) {
            throw new InvalidCheckException("Name is invalid or empty/Null");
        }
    }
}
