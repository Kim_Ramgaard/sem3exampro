/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.RMIServer;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rasmus
 */
public class RMIServer {

    private static RMIServer instance;
    private static IScoreBoard scoreBoard;

    private RMIServer() {
        try {
            LocateRegistry.createRegistry(1099);
            scoreBoard = new ScoreBoardImpl();
            Naming.rebind("rmi://localhost/TournamentService", scoreBoard);
            System.out.println("Server is running...");

        } catch (RemoteException | MalformedURLException ex) {
            Logger.getLogger(RMIServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static RMIServer getInstance() {
        if (instance == null) {
            instance = new RMIServer();
        }
        return instance;
    }

    public void shutdown() {
        try {
            Naming.unbind("rmi://localhost/TournamentService");
            UnicastRemoteObject.unexportObject(scoreBoard, true);
            System.out.println("Server has been shut down");
        } catch (RemoteException | NotBoundException | MalformedURLException ex) {
            Logger.getLogger(RMIServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
