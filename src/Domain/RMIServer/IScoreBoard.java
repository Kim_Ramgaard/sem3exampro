/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.RMIServer;

import Domain.Match;
import Domain.Team;
import Domain.Tournament;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rasmus
 */
public interface IScoreBoard extends Remote {
    
    public Map<Team, Integer> getScores(int matchID) throws RemoteException;
    
    public List<Match> getMatches(String tournamentName) throws RemoteException;
    
    public List<Tournament> getTournaments() throws RemoteException;
}
