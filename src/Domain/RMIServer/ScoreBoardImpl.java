/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.RMIServer;

import Domain.Match;
import Domain.Team;
import Domain.Tournament;
import ServiceLayer.ServiceLayerHandler;
import ServiceLayer.StorageException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rasmus
 */
public class ScoreBoardImpl extends UnicastRemoteObject implements IScoreBoard {

    public ScoreBoardImpl() throws RemoteException {
    }

    @Override
    public Map<Team, Integer> getScores(int matchID) throws RemoteException {
        Map<Team, Integer> scores = new HashMap<>();
        try {
            scores = ServiceLayerHandler.getInstance().getScores(matchID);
        } catch (StorageException ex) {
            Logger.getLogger(ScoreBoardImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scores;
    }

    @Override
    public List<Match> getMatches(String tournamentName) throws RemoteException {
        List<Match> matchList = new ArrayList<>();
        try {
            matchList = (List<Match>) ServiceLayerHandler.getInstance().get(Match.class.getName(), tournamentName);
        } catch (StorageException ex) {
            Logger.getLogger(ScoreBoardImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matchList;
    }

    @Override
    public List<Tournament> getTournaments() throws RemoteException {
        List<Tournament> tLst = new ArrayList();
        try {
            List<Object> oLst = ServiceLayerHandler.getInstance().getAll(Tournament.class.getSimpleName());
            for (Iterator<Object> iterator = oLst.iterator(); iterator.hasNext();) {
                tLst.add((Tournament) iterator.next());
            }
        } catch (StorageException ex) {
            Logger.getLogger(ScoreBoardImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tLst;

    }

}
