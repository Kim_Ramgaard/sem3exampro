/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.RMIServer;

/**
 *
 * @author Rasmus
 */
public class RMIServerHandler {
    
    private static RMIServerHandler instance;
    

    private RMIServerHandler() {
    }
    
    public static RMIServerHandler getInstance(){
        if(instance == null){
            instance = new RMIServerHandler();
        }
        return instance;
    }
    
    public void startupServer() {
        RMIServer.getInstance();
    }

    public void shutdownServer() {
        RMIServer.getInstance().shutdown();
    }

}
