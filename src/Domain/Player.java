package Domain;

import java.io.Serializable;

/**
 *
 * @author Nour
 */
public class Player implements Serializable
{
    private String firstName;
    private String lastName;
    private String schoolClass;
    
    //overloaded constructor
    public Player(String firstName, String lastName, String schoolClass)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.schoolClass = schoolClass;
    }
    
    //accessors and mutators
    public String getFirstName()
    {
        return firstName;
    }
    
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    
    public String getLastName()
    {
        return lastName;
    }
    
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    
    public String getSchoolClass()
    {
        return schoolClass;
    }
    
    public void setSchoolClass(String schoolClass)
    {
        this.schoolClass = schoolClass;
    }

    @Override
    public String toString() {
        return "Player{" + "firstName=" + firstName + ", lastName=" + lastName + ", schoolClass=" + schoolClass + '}';
    }
    
}