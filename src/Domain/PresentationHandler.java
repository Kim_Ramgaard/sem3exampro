/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import ServiceLayer.ServiceLayerHandler;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject.Prioity;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Singleton, handle controls from presentationLayer
 *
 * @author Kim Ramgaard
 */
public class PresentationHandler {

    private PresentationHandler() {
    }

    public void storeTournament(Tournament t) {
        ServiceLayerHandler.getInstance().store(t, t.getName(), Prioity.HIGH);
    }

    public void storeSignup(SignUp signup) {
        ServiceLayerHandler.getInstance().store(signup, signup.getTeam().getTeamName(), Prioity.HIGH);
    }

    public void StoreTimeMatches(List<Match> matchList) {
        ServiceLayerHandler.getInstance().store(matchList, "matchList");

    }

    public List<String> getSportnames() throws StorageException {
        List<Object> oLst = ServiceLayerHandler.getInstance().getAll("Sport");
        List<String> SportList = new ArrayList();
        for (Iterator<Object> iterator = oLst.iterator(); iterator.hasNext();) {
            SportList.add((String) iterator.next());
        }
        return SportList;
    }

    public Tournament getTournament(String key) throws StorageException {
        return (Tournament) ServiceLayerHandler.getInstance().get("Tournament", key);
    }

    public List<Tournament> getTournaments() throws StorageException {
        List<Object> oLst = ServiceLayerHandler.getInstance().getAll(Tournament.class.getSimpleName());
        List<Tournament> tLst = new ArrayList();
        for (Iterator<Object> iterator = oLst.iterator(); iterator.hasNext();) {
            tLst.add((Tournament) iterator.next());
        }
        return tLst;
    }

    public List<String> getRuleSets() throws StorageException {
        List<Object> oLst = ServiceLayerHandler.getInstance().getAll("RuleSet");
        List<String> ruleList = new ArrayList();
        for (Iterator<Object> iterator = oLst.iterator(); iterator.hasNext();) {
            ruleList.add((String) iterator.next());
        }
        return ruleList;
    }

    public List<Team> getTeams(String tournamentName) throws StorageException {
        return (List<Team>)ServiceLayerHandler.getInstance().get(Team.class.getName(),tournamentName);
        
    }

    public static PresentationHandler getInstance() {
        return GUIHandlerHolder.INSTANCE;
    }

    private static class GUIHandlerHolder {

        private static final PresentationHandler INSTANCE = new PresentationHandler();
    }
}
