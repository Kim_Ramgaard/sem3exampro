package Domain;

/**
 *
 * @author Nour
 */
public class InvalidCheckException extends Exception
{
    public InvalidCheckException(String message)
    {
        super(message);
    }
}
