package Domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nour
 */
public class Team implements Serializable
{
    private String teamName;
    private List<Player> players;
    private TeamManager teamManager;
    private String poolName;
    private int Score;
    
    //TESTING PURPOSES
    public Team(String teamName){
        this.teamName = teamName;
    }

    public Team(String teamName, int Score) {
        this.teamName = teamName;
        this.Score = Score;
    }

    public Team(String teamName, List<Player> players, TeamManager teamManager) {
        this.teamName = teamName;
        this.players = players;
        this.teamManager = teamManager;
    }

    public Team(String teamName, List<Player> players, TeamManager teamManager, String poolName) {
        this.teamName = teamName;
        this.players = players;
        this.teamManager = teamManager;
        this.poolName = poolName;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
    
    
 

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int Score) {
        this.Score = Score;
    }
    
    //accessors and mutators
    public String getTeamName()
    {
        return teamName;   
    }
    
    public void setTeamName(String teamName)
    {
        this.teamName = teamName;
    }
    

    public TeamManager getTeamManager() {
        return teamManager;
    }

    public void setTeamManager(TeamManager teamManager) {
        this.teamManager = teamManager;
    }

    @Override
    public String toString() {
        return "Team{" + "teamName=" + teamName + ", players=" + players + ", teamManager=" + teamManager + ", poolName=" + poolName + ", Score=" + Score + '}';
    }
    
    public void addTeam (Player p){
        if(players == null){
            players = new ArrayList();
        }
        players.add(p);
    }
    
    
    
    
    
}