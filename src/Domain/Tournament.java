package Domain;

import ServiceLayer.ServiceLayerHandler;
import ServiceLayer.StorageException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * information expert; A sports Tournament
 *
 * @author Kim Ramgaard
 */
public class Tournament implements Serializable {

    private String name;
    private String sport;
    private double signupfee;
    private Date startDate;
    private Date endDate;
    private List<Match> matches;
    private IRuleSet ruleSet;
    private String ruleName;
    private String ruleDescription;
    private List<Team> teams;

    public Tournament(String name, String sport, double signupfee, Date startDate, Date endDate, List<Match> matches, String ruleName, String ruleDescription, List<Team> teams) {
        this.name = name;
        this.sport = sport;
        this.signupfee = signupfee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.matches = matches;
        setRuleSet(ruleName);
        this.ruleDescription = ruleDescription;
        this.teams = teams;
    }

    public Tournament(String name, String sport, double signupfee, Date startDate, Date endDate, List<Match> matches, String ruleName, String ruleDescription) {
        this.name = name;
        this.sport = sport;
        this.signupfee = signupfee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.matches = matches;
        setRuleSet(ruleName);
        this.ruleDescription = ruleDescription;
    }

    public Tournament(String name, String sport, double signupfee, Date startDate, Date endDate) {
        this.name = name;
        this.sport = sport;
        this.signupfee = signupfee;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public void makeMatches() throws TournamentException, StorageException {
        matches = ruleSet.createMatches(teams);
        ServiceLayerHandler.getInstance().setMatches(matches, name);
    }

    public List<Match> getMatches() {
        return matches;
    }

    @Override
    public String toString() {
        return "Tournament{" + "name=" + name + ", sport=" + sport + ", signupfee=" + signupfee + ", startDate=" + startDate + ", endDate=" + endDate + ", matches=" + matches + ", ruleSet=" + ruleSet + ", ruleName=" + ruleName + ", ruleDescription=" + ruleDescription + '}';
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public double getSignupfee() {
        return signupfee;
    }

    public void setSignupfee(double signupfee) {
        this.signupfee = signupfee;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public IRuleSet getRuleSet() {
        return ruleSet;
    }

    public void setRuleSet(String rulesetName) {
        this.ruleName = rulesetName;
        if (rulesetName.toLowerCase().contains("pool")) {
            ruleSet = new PoolRule();
        }
    }

    public String getRuleName() {
        return ruleName;
    }

    public String getRuleDescription() {
        return ruleDescription;
    }

    public void setRuleDescription(String ruleDescription) {
        this.ruleDescription = ruleDescription;
    }

}
