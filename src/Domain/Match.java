package Domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kim Ramgaard
 */
public class Match implements Serializable {

    private int matchID;
    private List<Team> teams;
    private Date start;
    private Date end;

    public int getMatchID() {
        return matchID;
    }

    public void setMatchID(int matchID) {
        this.matchID = matchID;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void addTeam(Team t) {
        if (teams == null) {
            teams = new ArrayList();
        }
        teams.add(t);
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Match{" + "matchID=" + matchID + ", teams=" + teams + ", start=" + start + ", end=" + end + '}';
    }

}
