package Domain;

import java.io.Serializable;

/**
 *
 * @author Nour
 */
public class SignUp implements Serializable
{
    private Team team;
    
    //overloaded constructor
    public SignUp(Team team)
    {
        this.team = team;
    }
    
    //accessors and mutators
    public Team getTeam()
    {
        return team;
    }
    
    public void setTeam(Team team)
    {
        this.team = team;
    }
    
//    public void makeSignUp(SignUp signup) 
//    {
//
//    }
}