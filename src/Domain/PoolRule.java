package Domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * calculate matches for a pool tournament
 *
 * @author Nour
 */
public class PoolRule implements IRuleSet, Serializable {

    @Override
    public List<Match> createMatches(List<Team> teams) throws TournamentException {
        checkTeam(teams);

        return makeMatches(teams);
    }

    public List<PoolMatch> createPoolMatches(List<Team> teams, String poolName) {

        List<PoolMatch> matches = new ArrayList();

        int offset = 1;
        for (int i = 0; i < teams.size(); i++) {
            for (int j = 0; j < teams.size() - offset; j++) {
                matches.add(new PoolMatch(teams.get(i), teams.get(j + offset), poolName));
            }
            offset++;
        }
        return matches;
    }

    /**
     * test if the number of team meets the requirement
     *
     * @return true if the tournament is possible
     */
    private boolean checkTeam(List<Team> teams) throws TournamentException {
        if (teams.size() < 3) {
            throw new TournamentException("There are too few teams to make a tournament");
        } else if (teams.size() > 24) {
            throw new TournamentException("There are too many teams to make a tournament");
        }
        return true;
    }

    /**
     * Shuffles teams and split them into pools with a maximum of 1 in size
     * difference
     *
     * @param teams
     * @return List
     * @throws TournamentException
     */
    public List<Match> makeMatches(List<Team> teams) throws TournamentException {

        Collections.shuffle(teams);

        int n = teams.size();
        
        List<Match> matches = new ArrayList<>();
        
        
        
      

        try {
            if (n >= 3 && n <= 5) {
                for (int i = 0; i < teams.size(); i++) {
                    teams.get(i).setPoolName("Pool A");
                }
                
                matches.addAll(PoolRule.this.createPoolMatches(teams,"Pool A"));

//                //TESTING
//                for (int i = 0; i < teams.size(); i++) {
//                    System.out.println("Pool: " + teams.get(i).getPoolName() + "\n\tTeam: " + teams.get(i).getTeamName());
//                }
            }
            if (n >= 6 && n <= 11) {
                int firstPoolLength = teams.size() / 2;
                int secondPoolLength = teams.size() - firstPoolLength;
                List<Team> teamA = new ArrayList();
                List<Team> teamB = new ArrayList();

                teams.get(0).setPoolName("Pool A");
                teamA.add(teams.get(0));
                teams.remove(0);

                for (int i = 0; i < teams.size(); i++) {
                    for (int j = 0; j < teamA.size() && teamA.size() < firstPoolLength; j++) {
                        if (comparePlayerclass(teamA.get(j), teams.get(i))) {
                            teams.get(i).setPoolName("Pool A");
                            teamA.add(teams.get(i));
                            teams.remove(i);
                        }
                    }
                }


                teams.get(0).setPoolName("Pool B");
                teamB.add(teams.get(0));
                teams.remove(0);

                for (int i = 0; i < teams.size(); i++) {
                    for (int j = 0; j < teamB.size() && teamB.size() < secondPoolLength; j++) {
                        if (comparePlayerclass(teamB.get(j), teams.get(i))) {
                            teams.get(i).setPoolName("Pool B");
                            teamB.add(teams.get(i));
                            teams.remove(i);
                        }
                    }
                }

                while (teamA.size() < firstPoolLength) {
                    teams.get(0).setPoolName("Pool A");
                    teamA.add(teams.get(0));
                    teams.remove(0);
                }
                matches.addAll(PoolRule.this.createPoolMatches(teamA, "Pool A"));

                while (teamB.size() < secondPoolLength) {
                    teams.get(0).setPoolName("Pool B");
                    teamB.add(teams.get(0));
                    teams.remove(0);
                }
                
                matches.addAll(PoolRule.this.createPoolMatches(teamB, "Pool B"));
//                for (int i = 0; i < teamA.size(); i++) {
//                    teams.add(teamA.get(i));
//                }
//                for (int i = 0; i < teamB.size(); i++) {
//                    teams.add(teamB.get(i));
//                }

//
//                //TESTING
//                for (int i = 0; i < teams.size(); i++) {
//                    System.out.println("Pool: " + teams.get(i).getPoolName() + "\n\tTeam: " + teams.get(i).getTeamName());
//                }
            }
            if (n >= 12 && n <= 24) {
                int firstPoolLength = teams.size() / 4;
                int secondPoolLength = (teams.size() - firstPoolLength) / 3;
                int thirdPoolLength = (teams.size() - (firstPoolLength + secondPoolLength)) / 2;
                int fourthPoolLength = teams.size() - (firstPoolLength + secondPoolLength + thirdPoolLength);
                List<Team> teamA = new ArrayList();
                List<Team> teamB = new ArrayList();
                List<Team> teamC = new ArrayList();
                List<Team> teamD = new ArrayList();

                teams.get(0).setPoolName("Pool A");
                teamA.add(teams.get(0));
                teams.remove(0);

                for (int i = 0; i < teams.size(); i++) {
                    for (int j = 0; j < teamA.size() && teamA.size() < firstPoolLength; j++) {
                        if (comparePlayerclass(teamA.get(j), teams.get(i)) && comparePlayerclass(teamA.get(0), teams.get(i))) {
                            teams.get(i).setPoolName("Pool A");
                            teamA.add(teams.get(i));
                            teams.remove(i);
                        }
                    }
                }

                teams.get(0).setPoolName("Pool B");
                teamB.add(teams.get(0));
                teams.remove(0);

                for (int i = 0; i < teams.size(); i++) {
                    for (int j = 0; j < teamB.size() && teamB.size() < secondPoolLength; j++) {
                        if (comparePlayerclass(teamB.get(j), teams.get(i)) && comparePlayerclass(teamB.get(0), teams.get(i))) {
                            teams.get(i).setPoolName("Pool B");
                            teamB.add(teams.get(i));
                            teams.remove(i);
                        }
                    }
                }
                teams.get(0).setPoolName("Pool C");
                teamC.add(teams.get(0));
                teams.remove(0);

                for (int i = 0; i < teams.size(); i++) {
                    for (int j = 0; j < teamC.size() && teamC.size() < thirdPoolLength; j++) {
                        if (comparePlayerclass(teamC.get(j), teams.get(i)) && comparePlayerclass(teamC.get(0), teams.get(i))) {
                            teams.get(i).setPoolName("Pool C");
                            teamC.add(teams.get(i));
                            teams.remove(i);
                        }
                    }
                }

                teams.get(0).setPoolName("Pool D");
                teamD.add(teams.get(0));
                teams.remove(0);

                for (int i = 0; i < teams.size(); i++) {
                    for (int j = 0; j < teamD.size() && teamD.size() < fourthPoolLength; j++) {
                        if (comparePlayerclass(teamD.get(j), teams.get(i)) && comparePlayerclass(teamD.get(0), teams.get(i))) {
                            teams.get(i).setPoolName("Pool D");
                            teamD.add(teams.get(i));
                            teams.remove(i);
                        }
                    }
                }

                while (teamA.size() < firstPoolLength) {
                    teams.get(0).setPoolName("Pool A");
                    teamA.add(teams.get(0));
                    teams.remove(0);
                }
                matches.addAll(PoolRule.this.createPoolMatches(teamA, "Pool A"));
                
                while (teamB.size() < secondPoolLength) {
                    teams.get(0).setPoolName("Pool B");
                    teamB.add(teams.get(0));
                    teams.remove(0);
                }
                matches.addAll(PoolRule.this.createPoolMatches(teamB, "Pool B"));
                
                while (teamC.size() < thirdPoolLength) {
                    teams.get(0).setPoolName("Pool C");
                    teamC.add(teams.get(0));
                    teams.remove(0);
                }
                matches.addAll(PoolRule.this.createPoolMatches(teamC, "Pool C"));
                
                while (teamD.size() < fourthPoolLength) {
                    teams.get(0).setPoolName("Pool D");
                    teamD.add(teams.get(0));
                    teams.remove(0);
                }
                matches.addAll(PoolRule.this.createPoolMatches(teamD, "Pool D"));
                

//                for (int i = 0; i < teamA.size(); i++) {
//                    teams.add(teamA.get(i));
//                }
//                for (int i = 0; i < teamB.size(); i++) {
//                    teams.add(teamB.get(i));
//                }
//                for (int i = 0; i < teamC.size(); i++) {
//                    teams.add(teamC.get(i));
//                }
//                for (int i = 0; i < teamD.size(); i++) {
//                    teams.add(teamD.get(i));
//                }

//                for (int i = 0; i < teams.size(); i++) {
//                    while (i < firstPoolLength) {
//                        teams.get(i).setPoolName("Pool A");
//                        break;
//                    }
//                    while (i >= firstPoolLength && i < firstPoolLength + secondPoolLength) {
//                        teams.get(i).setPoolName("Pool B");
//                        break;
//                    }
//                    while (i >= firstPoolLength + secondPoolLength && i < firstPoolLength + secondPoolLength + thirdPoolLength) {
//                        teams.get(i).setPoolName("Pool C");
//                        break;
//                    }
//                    while (i >= firstPoolLength + secondPoolLength + thirdPoolLength && i < firstPoolLength + secondPoolLength + thirdPoolLength + fourthPoolLength) {
//                        teams.get(i).setPoolName("Pool D");
//                        break;
//                    }
//
//                }
                //TESTING
                for (int i = 0; i < teams.size(); i++) {
                    System.out.println("Pool: " + teams.get(i).getPoolName() + "\n\tTeam: " + teams.get(i).getTeamName());
                }
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new TournamentException("Failed to make pools");
        }
        
        
        return matches;
    }

    /**
     * check if two teams have players from the same class
     *
     * @param t1 first team to check
     * @param t2 next team to check
     * @return true if the teams don't have a player from the same class
     */
    private boolean comparePlayerclass(Team t1, Team t2) {
        List<Player> t1Players = t1.getPlayers();
        List<Player> t2Players = t2.getPlayers();

        for (int i = 0; i < t1Players.size(); i++) {
            for (int j = 0; j < t2Players.size(); j++) {
                if (t1Players.get(i).getSchoolClass().equals(t2Players.get(j).getSchoolClass())) {
                    return false;
                }
            }
        }
        return true;
    }
}
