package Domain;

import java.util.List;

/**
 *
 * @author Nour
 */
public interface IRuleSet {
    public List<Match> createMatches(List<Team> teams) throws TournamentException;
}
