
package ServiceLayer;

/**
 * made to make sure of consistency between handlers
 * @author Kim Ramgaard
 */
public interface IServiceHandler {
    
    /**
     * startup of the storage system
     * @throws StorageException 
     */
    public void Startup() throws StorageException;
    
    /**
     * shut down the storage
     * @throws StorageException 
     */
    public void shutdown() throws StorageException;
    /**
     * store a single object in storage
     * @param so the stored object to be stored
     * @throws StorageException 
     */
    public void store(StoredObject so) throws StorageException;
    
    /**
     * get a single stored object from storage
     * @param className classname of the object that should be get
     * @param key the primary key of the object
     * @return a stored object, can be used for storage in other storagesystems 
     * @throws StorageException 
     */
    public StoredObject get(String className, String key) throws StorageException;
    
        
}
