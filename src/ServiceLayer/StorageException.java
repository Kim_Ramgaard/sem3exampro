package ServiceLayer;

/**
 * is thrown from the serviceLayer indicating a problem with the storage.
 * @author Kim Ramgaard
 */
public class StorageException extends Exception {

    /**
     * Creates a new instance of <code>StorageException</code> without detail
     * message.
     */
    public StorageException() {
//        ServiceLayerHandler.getInstance().shutdown();
//        ServiceLayerHandler.getInstance().startup();
        
    }

    /**
     * Constructs an instance of <code>StorageException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public StorageException(String msg) {
        super(msg);
//        ServiceLayerHandler.getInstance().shutdown();
//        ServiceLayerHandler.getInstance().startup();
    }
}
