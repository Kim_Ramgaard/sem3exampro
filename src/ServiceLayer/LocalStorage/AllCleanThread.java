/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.LocalStorage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kim Ramgaard
 */
public class AllCleanThread implements Runnable {

    @Override
    public void run() {

        LocalStorage ls = new LocalStorage();
        ls.checkAllFiles();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(AllCleanThread.class.getName()).log(Level.SEVERE, "clean thread thread intercepted", ex);
        }

    }

}
