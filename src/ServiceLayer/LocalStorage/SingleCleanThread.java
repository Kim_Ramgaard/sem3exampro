package ServiceLayer.LocalStorage;

import ServiceLayer.ServiceLayerHandler;
import ServiceLayer.StoredObject;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * a thread that cleans a single file for expried objects
 *
 * @author Kim Ramgaard
 */
public class SingleCleanThread extends Thread {

    private File file;

    public SingleCleanThread(File file) {
        this.file = file;
    }

    @Override
    public void run() {
        while (ServiceLayerHandler.getInstance().isRunThreads()) {
            //check if file exist then clean the file and then sleep
            if (file.exists()) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SingleCleanThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                cleanFile();
            }
        }
    }

    private synchronized int cleanFile() {
        //used to count number of deleted entires 
        int count = 0;
        try {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));

            //get map from file, set iteretor, and make a new date to represent current time
            Map<String, StoredObject> map = (HashMap) input.readObject();
            Iterator it = map.entrySet().iterator();
            Date currentTime = new Date();
            //iterate throug map to find old values
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry<String, StoredObject>) it.next();
                StoredObject so = (StoredObject) pair.getValue();
                if (so.getTimeToLive().after(currentTime)) {
                    it.remove();
                    count++;
                }
            }
            //checks if map is empty, if empty then it delete the file if not then it write the new file to storage
            if (map.isEmpty()) {
                input.close();
                file.delete();
            } else {
                ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(file, false));
                output.writeObject(map);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "deleted local storage in middle of process", ex);
        } catch (IOException ex) {
            Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "file corrupted", ex);
        }

        return count;

    }

}
