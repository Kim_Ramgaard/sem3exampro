package ServiceLayer.LocalStorage;

import ServiceLayer.StoredObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * stores and recieves information on the local server
 *
 * @author Kim Ramgaard
 */
public class LocalStorage {

    private final String LOCAL_STORAGE_PATH = "Local_Storage/";

    private Map<String, StoredObject> storageMap;

    private ObjectOutputStream output;
    private ObjectInputStream input;

//    private int localSize;
//    private int localLoadFactor;
    public LocalStorage() {
        //get inforamtion about size and load factor from propertyile and then set map (default = 100,10)
//        Properties cacheInfo = PropertyAdapter.getInstance().getProperties();
//        localSize = Integer.parseInt(cacheInfo.getProperty("Local_Size", "100"));
//        localLoadFactor = Integer.parseInt(cacheInfo.getProperty("Local_LoadFactor", "10"));

    }

    /**
     * writes object to file
     *
     * @param storedObject
     */
    public void store(StoredObject storedObject) {

        try {
            String objectName = storedObject.getObject().getClass().getName().replace(".", "");

            File file = new File(LOCAL_STORAGE_PATH + objectName);
            boolean fileExist = file.exists();
            file.createNewFile();

            //check if file exist; if true: get map from file; if false makes a new Hashmap and starts clean thread
            if (fileExist) {
                //Object o = input.readObject();
                input = new ObjectInputStream(new FileInputStream(file));
                storageMap = (HashMap) input.readObject();
            } else {
                storageMap = new HashMap<>();
                Thread cleanThread = new SingleCleanThread(file);
                cleanThread.start();
            }
            storageMap.put(storedObject.getKey(), storedObject);

            output = new ObjectOutputStream(new FileOutputStream(file, false));
            output.writeObject(storageMap);
            output.flush();

        } catch (IOException | ClassNotFoundException ex) {
            
        }

    }

    /**
     * get object with given key
     *
     * @param className Name of the class that is searched for
     * @param key primary key to be searched for
     * @return the object, return null if not found
     */
    public StoredObject getStoredObject(String className, String key) {
        try {
            String fullKey = className + ":" + key;

            input = new ObjectInputStream(new FileInputStream(LOCAL_STORAGE_PATH + className.replace(".", "")));

            storageMap = (HashMap) input.readObject();

            StoredObject so = (StoredObject) storageMap.get(fullKey);

            return so;

        } catch (IOException ex) {
            try {
                input.close();
            } catch (IOException ex1) {
                Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "could not close connection", ex1);
            }
            Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "ioexception", ex);

        } catch (ClassNotFoundException ex) {
            try {
                input.close();
            } catch (IOException ex1) {
                Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "could not close connection", ex1);
            }
            Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "class missmatch", ex);
        }
        return null;
    }

    /**
     * used for checking all files in local storage, if they are expired
     *
     * @return the number of deleted entries
     */
    public synchronized int checkAllFiles() {
        File storagePath = new File(LOCAL_STORAGE_PATH);

        String[] files = storagePath.list();

        //used to count number of deleted entires 
        int count = 0;

        //iterate through allfiles in LOCAL_STORAGE_PATH
        for (String file : files) {
            try {
                File f = new File(LOCAL_STORAGE_PATH, file);
                input = new ObjectInputStream(new FileInputStream(f));

                //get map from file, set iteretor, and make a new date to represent current time
                Map<String, StoredObject> map = (HashMap) input.readObject();
                Iterator it = map.entrySet().iterator();
                Date currentTime = new Date();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry<String, StoredObject>) it.next();
                    StoredObject so = (StoredObject) pair.getValue();
                    if (so.getTimeToLive().before(currentTime)) {
                        it.remove();
                        count++;
                    }
                }
                //checks if map is empty, if empty then it delete the file if not then it write the new file to storage
                if (map.isEmpty()) {
                    input.close();
                    f.delete();
                } else {
                    output = new ObjectOutputStream(new FileOutputStream(f, false));
                    output.writeObject(map);
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "deleted local storage in middle of process", ex);
            } catch (IOException ex) {
                Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(LocalStorage.class.getName()).log(Level.SEVERE, "file corrupted", ex);
            }
        }
        return count;
    }

//    public void setMainValues() throws StorageException {
//        try {
//            IServiceHandler[] handlers = ServiceLayerHandler.getInstance().getHandlers();
//            DatabaseHandler DBHandler = null;
//            for (int i = 0; i < handlers.length; i++) {
//                if (handlers[i] instanceof DatabaseHandler) {
//                    DBHandler = (DatabaseHandler) handlers[i];
//                }
//            }
//
//            List<StoredObject> storedObjectList = DBHandler.getMainValues();
//
//            for (int i = 0; i < storedObjectList.size(); i++) {
//                String className = storedObjectList.get(i).getKey().split(":", 3)[1];
//                if (storedObjectList.get(i).getKey().split(":", 3)[1].equals(className)) {
//                    File file = new File(LOCAL_STORAGE_PATH + className);
//                    boolean fileExist = file.exists();
//                    file.createNewFile();
//                    if (fileExist) {
//                        //Object o = input.readObject();
//                        input = new ObjectInputStream(new FileInputStream(file));
//                        storageMap = (HashMap) input.readObject();
//                    } else {
//                        storageMap = new HashMap<>();
//                        Thread cleanThread = new SingleCleanThread(file);
//                        cleanThread.start();
//                    }
//
//                    for (int j = 0; j < storedObjectList.size(); j++) {
//                        String currentClass = storedObjectList.get(j).getKey().split(":", 3)[1];
//                        if (currentClass == null ? className == null : currentClass.equals(className)) {
//                            storageMap.put(storedObjectList.get(j).getKey().split(":", 3)[1], storedObjectList.get(i));
//                        }
//                    }
//                    output = new ObjectOutputStream(new FileOutputStream(file, false));
//                    output.writeObject(storageMap);
//                    output.flush();
//                }
//
//            }
//
//            //check if file exist; if true: get map from file; if false makes a new Hashmap and starts clean thread
//        } catch (IOException ex) {
//            throw new StorageException("could not write main values to file");
//        } catch (ClassNotFoundException ex) {
//            throw new StorageException("could not find Main_Values file");
//        }
//    }
}