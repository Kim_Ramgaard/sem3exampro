/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.LocalStorage;

import ServiceLayer.IServiceHandler;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;

/**
 *
 * @author Kim Ramgaard
 */
public class LocalStorageHandler implements IServiceHandler{
    
    
    @Override
    public void Startup() throws StorageException {
        
        LocalStorage ls = new LocalStorage();
        ls.checkAllFiles();
        //ls.setMainValues();
    }

    @Override
    public void shutdown() {
       
    }
        
    /**
     * store the specified cached object on the local drive
     * @param obj the cached object 
     */
    @Override
    public void store(StoredObject obj){
        new LocalStorage().store(obj);
    }
    
    /**
     * try to find the specified object on the local drive
     * @param className classname of the object that you wish to recieve
     * @param key the specified key to identify the object
     * @return the object
     */
    @Override
    public StoredObject get(String className, String key){
        return new LocalStorage().getStoredObject(className, key);
    }
    
    
    
    
    /**
     * start a thread with low prioity to reload the default data
     */
    public void reloadData(){
     //TODO   
    }
}
