/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * stores an object in the cache with extra parameters
 *
 * @author Kim Ramgaard
 */
public class StoredObject implements Serializable {

    private Object object;
    private String key;
    private Date timeToLive;

    public enum Prioity {
        LOW, MID, HIGH
    };

    public Object getObject() {
        return object;
    }

    public String getKey() {
        return key;
    }

    public Date getTimeToLive() {
        return timeToLive;
    }
    
    

    /**
     * stored object is used when an object is stored either in cache or local
 storage. the Prioity enum decides when the garbage collector will discard
 the object. (default is 12 hours)
     *
     * @param object the object that should be stored
     * @param key the key wich to store the object
     * @param prioity the Prioity of the stored object
     */
    public StoredObject(Object object, String key, Prioity prioity) {
        this.object = object;
        this.key = object.getClass().getName() + ":" + key;

        Calendar c = Calendar.getInstance();

        switch (prioity) {
            case LOW:
                c.add(Calendar.HOUR, 1);
                break;
            case MID:
                c.add(Calendar.HOUR, 12);
                break;
            case HIGH:
                c.add(Calendar.HOUR, 24);
                break;
            default:
                c.add(Calendar.HOUR, 12);
                break;
        }
        timeToLive = c.getTime();

    }


}
