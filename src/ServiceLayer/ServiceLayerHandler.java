package ServiceLayer;

import Domain.Match;
import Domain.Team;
import ServiceLayer.Cache.CacheHandler;
import ServiceLayer.Database.DB_Match;
import ServiceLayer.Database.DatabaseHandler;
import ServiceLayer.StoredObject.Prioity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Kim Ramgaard
 */
public class ServiceLayerHandler {

    private IServiceHandler[] handlers;
    private boolean runThreads;

    private ServiceLayerHandler() {
        //add handlers to look for data IMPORTANT: have to be added in the order they should be searched in
        handlers = new IServiceHandler[2];
        //handlers[0] = new CacheHandler();
        //handlers[1] = new LocalStorageHandler();
        handlers[0] = new CacheHandler();
        handlers[1] = new DatabaseHandler();
        runThreads = true;

    }

    public IServiceHandler[] getHandlers() {
        return handlers;
    }

    public boolean isRunThreads() {
        return runThreads;
    }

    /**
     * will search cache, local storage and database (in that order) for the
     * specific object.
     *
     * @param className specific class name that is being looked for.
     * @param key the key that should be looked for. preferrebly the primary
     * key.
     * @return an object that needs to be cast to the prefered object type.
     * return null if not found
     * @throws ServiceLayer.StorageException
     */
    public Object get(String className, String key) throws StorageException {
        StoredObject storedObject = null;
        for (int i = 0; i < handlers.length && storedObject == null; i++) {
            storedObject = handlers[i].get(className, key);
        }

        if (!(storedObject.equals(null))) {
            StoreThread st = new StoreThread(storedObject, handlers, false);
            st.start();
            return storedObject.getObject();
        }
        return null;
    }
    
    public void setMatches(List<Match> matches, String tournamentName) throws StorageException{
        DatabaseHandler dh = new DatabaseHandler();
        dh.setMatches(matches, tournamentName);
    }

    /**
     * get all objects with the given classname
     *
     * @param className name of the class to get all objects from
     * @return all objects that is of the given classtype
     * @throws StorageException
     */
    public List<Object> getAll(String className) throws StorageException {
        for (int i = 0; i < handlers.length; i++) {
            if (handlers[i] instanceof DatabaseHandler) {
                DatabaseHandler dh = (DatabaseHandler) handlers[i];
                List<StoredObject> soLst = dh.getAll(className);
                List<Object> oLst = new ArrayList();

                for (int j = 0; j < soLst.size(); j++) {
                    oLst.add(soLst.get(j).getObject());
                }
                return oLst;
            }
        }
        return null;
    }

    /**
     * manually store an object in the service layer (this will be done
     * automatically when an object is fetched from servicelayer)
     *
     * @param o the object to be stored
     * @param key the primary key
     * @param prioity Prioity of the stored object
     */
    public void store(Object o, String key, Prioity prioity) {
        new StoreThread(new StoredObject(o, key, prioity), handlers, true).start();
    }

    /**
     * manually store an object in the service layer, prioity is store to
     * default (this will be done automatically when an object is fetched from
     * servicelayer)
     *
     * @param o the object to be stored
     * @param key the primary key
     */
    public void store(Object o, String key) {
        new StoreThread(new StoredObject(o, key, Prioity.MID), handlers, true).start();
    }

    /**
     * TODO; will prep the servicelayer (store default thing in local
     * storage,clean old values)
     */
    public void startup() throws StorageException {
        for (int i = 0; i < handlers.length; i++) {
            handlers[i].Startup();
        }
    }

    /**
     * TODO; will prep for closing the servicelayer(close connection, store
     * important objects from cache)
     */
    public void shutdown() throws StorageException {

        for (int i = 0; i < handlers.length; i++) {
            handlers[i].shutdown();
        }
        runThreads = false;
    }

    public Map<Team, Integer> getScores(int matchID) throws StorageException {
        DB_Match matchHandler = new DB_Match();
        return matchHandler.getScores(matchID);
    }

    //public void Pending() throws 
    public static ServiceLayerHandler getInstance() {
        return ServiceLayerHandlerHolder.INSTANCE;

    }

    private static class ServiceLayerHandlerHolder {

        private static final ServiceLayerHandler INSTANCE = new ServiceLayerHandler();
    }
}
