package ServiceLayer;

import ServiceLayer.Database.DatabaseHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread that stores a stored Object in cache, on local and in database.
 *
 * @author Kim Ramgaard
 */
public class StoreThread extends Thread {

    private StoredObject storedObject;
    private IServiceHandler[] handlers;
    private boolean storeInDatabase;

    public StoreThread(StoredObject storedObject, IServiceHandler[] handlers, boolean storeInDatabase) {
        this.storedObject = storedObject;
        this.handlers = handlers;
        this.storeInDatabase = storeInDatabase; 
    }

    @Override
    public void run() {
        if (storeInDatabase) {
            storeWithDatabase();
        } else {
            storeWithoutDatabase();
        }

    }

    private void storeWithDatabase(){
        for (int i = 0; i < handlers.length; i++) {
            try {
                handlers[i].store(storedObject);
            } catch (StorageException ex) {
                Logger.getLogger(StoreThread.class.getName()).log(Level.SEVERE, "could not store object", ex);
            }

        }
    }

    private void storeWithoutDatabase() {
        for (int i = 0; i < handlers.length; i++) {
            if (!(handlers[i] instanceof DatabaseHandler)) {
                try {
                    handlers[i].store(storedObject);
                } catch (StorageException ex) {
                    Logger.getLogger(StoreThread.class.getName()).log(Level.SEVERE, "could not store Object", ex);
                }
            }

        }
    }

}
