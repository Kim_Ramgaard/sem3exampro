package ServiceLayer.Database;

import Domain.Match;
import Domain.PoolMatch;
import Domain.Team;
import Domain.Tournament;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * handle databasecalls regarding tournament objects
 *
 * @author Kim Ramgaard
 */
public class DB_Tournament extends DatabaseAdapter {

    @Override
    public void store(StoredObject so) throws StorageException {
        Tournament tournament = (Tournament) so.getObject();
        String query = "";
        try {
            query = "INSERT INTO tblTournament(fldTournamentName, fldSportName, fldSignupFee, fldStartDate, fldEndDate, fldRuleSetName) VALUES ('"
                    + tournament.getName() + "','"
                    + tournament.getSport() + "',"
                    + tournament.getSignupfee() + ",'"
                    + new java.sql.Date(tournament.getStartDate().getTime()) + "','"
                    + new java.sql.Date(tournament.getStartDate().getTime()) + "','"
                    + tournament.getRuleName() + "')\n";

            String matchQuery = "";
            if (tournament.getMatches() != null) {
                List<Match> matches = tournament.getMatches();
                for (int i = 0; i < matches.size(); i++) {
                    if (matches.get(i) instanceof PoolMatch) {
                        PoolMatch p = (PoolMatch) matches.get(i);
                        query += "INSERT INTO tblMatch (fldStartTime, fldEndTime,fldPoolName,fldTournamentName) VALUES ('"
                                + new java.sql.Date(p.getStart().getTime()) + "','"
                                + new java.sql.Date(p.getEnd().getTime()) + "','"
                                + p.getPoolName() + "','" + tournament.getName() + "'\n";
                    } else {
                        Match m = matches.get(i);

                        matchQuery += "INSERT INTO tblMatch (fldStartTime, fldEndTime,fldTournamentName) VALUES ('"
                                + new java.sql.Date(m.getStart().getTime()) + "','"
                                + new java.sql.Date(m.getEnd().getTime()) + "','" + tournament.getName() + "'\n";
                    }

                }
            }

            Statement stmt = Connect.getInstance().getStatement();
            System.out.println(query);
            stmt.execute(query);

        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println(query);
            throw new StorageException("Could_not_store_DB");
        }
    }

    @Override
    public Object get(String key) throws StorageException {
        try {
            String matchquery = "SELECT * FROM tblMatch WHERE fldTournamentName =  '" + key + "';";
            Statement stmt = Connect.getInstance().getStatement();

            ResultSet rs = stmt.executeQuery(matchquery);
            List<Match> matchList = new ArrayList();

            while (rs.next()) {

                Match m = new Match();

                Timestamp timestamp = rs.getTimestamp("fldStartTime");
                m.setStart(new java.util.Date(timestamp.getTime()));

                timestamp = rs.getTimestamp("fldEndTime");
                m.setEnd(new java.util.Date(timestamp.getTime()));

                m.setMatchID(rs.getInt("fldMatchID"));
                matchList.add(m);

                String matchTeamQuery = "SELECT * FROM tblMatchTeams WHERE fldMatchID =  '" + rs.getString("fldMatchID") + "';";
                Statement matchteamStmt = Connect.getInstance().getStatement();
                ResultSet matchTeamRs = matchteamStmt.executeQuery(matchTeamQuery);

                while (matchTeamRs.next()) {
                    Team t = new Team(matchTeamRs.getString("fldTeamName"));
                    m.addTeam(t);

                }

            }

            String query = "SELECT * FROM tblTournament JOIN tblRuleSet ON (tblTournament.fldRuleSetName = tblRuleSet.fldRuleSetName)WHERE fldTournamentName =  '" + key + "';";

            stmt = Connect.getInstance().getStatement();
            rs = stmt.executeQuery(query);  
            rs.next();
            return new Tournament(key, rs.getString("fldSportName"), rs.getDouble("fldSignupFee"), rs.getDate("fldStartDate"), rs.getDate("fldEndDate"), matchList, rs.getString("fldRuleSetName"), rs.getString("fldDescription"));
            //return new Tournament(key, rs.getString("fldSportname"), rs.getDouble("fldSignupFee"), rs.getDate("fldStartDate"), rs.getDate("fldEndDate"));
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new StorageException("could not get tournament object");
        }

    }

    @Override
    public List<StoredObject> getAll() throws StorageException {
        try {
            String query = "SELECT * FROM tblTournament JOIN tblRuleSet ON (tblTournament.fldRuleSetName = tblRuleSet.fldRuleSetName)";

            Statement s = Connect.getInstance().getStatement();
            s.execute(query);

            ResultSet rs = s.getResultSet();
            List<Tournament> list = new ArrayList();

            while (rs.next()) {
                Tournament t = new Tournament(rs.getString("fldTournamentName"), rs.getString("fldSportName"), rs.getDouble("fldSignupFee"), rs.getDate("fldStartDate"), rs.getDate("fldEndDate"), null, rs.getString("fldRuleSetName"), rs.getString("fldDescription"));
                list.add(t);
            }

            StoredObject[] storedTournaments = new StoredObject[list.size()];
            List<StoredObject> soLst = new ArrayList();
            for (int i = 0; i < storedTournaments.length; i++) {
                soLst.add(new StoredObject(list.get(i), list.get(i).getName(), StoredObject.Prioity.MID));
            }

            return soLst;

        } catch (SQLException ex) {
            throw new StorageException("could not find tournaments");
        }

    }

}
