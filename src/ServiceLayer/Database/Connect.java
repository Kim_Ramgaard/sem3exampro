package ServiceLayer.Database;

import ServiceLayer.PropertyAdapter;
import ServiceLayer.StorageException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * a singleton method to get connection to the database
 *
 * @author Kim Ramgaard
 */
public class Connect {

    private Connection con;

    public Connection getCon() {
        return con;
    }

    private Connect() throws StorageException {
        try {
            Properties connectInfo = PropertyAdapter.getInstance().getProperties();

            String user = connectInfo.getProperty("DB_User");
            String password = connectInfo.getProperty("DB_Password");
            String connectionUrl = connectInfo.getProperty("DB_ConnectionUrl");
            String classforname = connectInfo.getProperty("DB_Classforname");

            Class.forName(classforname);
            con = DriverManager.getConnection(connectionUrl, user, password);
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            throw new StorageException("unable to connect to database");
        }
    }

    //get properties from prop.properties 
//    private static Properties getProperties() {
//        Properties p = new Properties();
//        try {
//            p.load(new FileInputStream("src/config.properties"));
//        } catch (IOException ex) {
//            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, "cannot get connection to config file", ex);
//        }
//
//        return p;
//    }

    //close connection to database
    protected void closeCon() {

        try {
            if (!con.isClosed()) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, "connection cannot be closed", ex);
        }

    }

    // make the connection to the database
    protected static Connect getInstance() throws StorageException {
        return ConnectHolder.getInstance();
    }

    /**
     * get a useable statement to the database
     *
     * @return statement
     * @throws DBConException connection issues with the database
     */
    protected Statement getStatement() throws StorageException {

        try {
            return ConnectHolder.getInstance().getCon().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException ex) {
            ConnectHolder.getInstance().closeCon();
            throw new StorageException("can not get statement, connection closed");
        }
    }

    /**
     * get a usable prepared statement
     *
     * @param query the sql-statement
     * @return prepared Statement
     * @throws DBConException connection issues with the database
     */
    protected PreparedStatement getPreparedStatement(String query) throws StorageException {

        try {
            return ConnectHolder.getInstance().getCon().prepareStatement(query);
        } catch (SQLException ex) {
            ConnectHolder.getInstance().closeCon();
            throw new StorageException("can not get prepared statement, connection closed");
        }
    }

    private static class ConnectHolder {

        private static Connect INSTANCE;

        private static Connect getInstance() throws StorageException {
            if (INSTANCE == null) {
                INSTANCE = new Connect();
            }
            return INSTANCE;
        }
    }
}
