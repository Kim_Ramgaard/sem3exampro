package ServiceLayer.Database;

import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kim Ramgaard
 */
public class DB_RuleSet extends DatabaseAdapter {

    @Override
    public void store(StoredObject so) throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object get(String key) throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StoredObject> getAll() throws StorageException {
        try {
            Statement stmt = Connect.getInstance().getStatement();
            ResultSet rs = stmt.executeQuery("Select fldRuleSetName AS RuleSet FROM tblRuleSet ");
            List<StoredObject> soList = new ArrayList();
            
            while(rs.next()){
                StoredObject so = new StoredObject(rs.getString("RuleSet"), "RuleSet:"+rs.getString("RuleSet"), StoredObject.Prioity.LOW);
                soList.add(so); 
            }
            
            return soList;
        } catch (SQLException ex) {
            Logger.getLogger(DB_RuleSet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
