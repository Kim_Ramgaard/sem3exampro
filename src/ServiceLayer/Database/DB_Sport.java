package ServiceLayer.Database;

import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kim Ramgaard
 */
public class DB_Sport extends DatabaseAdapter{

    @Override
    public void store(StoredObject so) throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object get(String key) throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StoredObject> getAll() throws StorageException {
        try {
            Statement stmt = Connect.getInstance().getStatement();
            ResultSet rs = stmt.executeQuery("Select fldSportName AS sportName FROM tblSport ");
            List<StoredObject> soList = new ArrayList();
            
            while(rs.next()){
                StoredObject so = new StoredObject(rs.getString("sportName"), "Sport:"+rs.getString("sportName"), StoredObject.Prioity.LOW);
                soList.add(so); 
            }
            
            return soList;
        } catch (SQLException ex) {
           throw new StorageException("could not get all sportsnames"); 
        }
    }
        
    
}
