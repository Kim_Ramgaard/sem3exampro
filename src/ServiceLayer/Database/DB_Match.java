/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.Database;

import Domain.Match;
import Domain.Team;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kim Ramgaard
 */
public class DB_Match extends DatabaseAdapter {

    @Override
    public void store(StoredObject so) throws StorageException {
        List<Match> matchList = (List<Match>) so.getObject();

        String query = "";

        for (int i = 0; i < matchList.size(); i++) {
            query += "UPDATE tblMatch (fldStartTime, fldEndTime) VALUES ('"
                    + new java.sql.Date(matchList.get(i).getStart().getTime()) + "','"
                    + new java.sql.Date(matchList.get(i).getEnd().getTime()) + "') WHERE fldMatchID = '"
                    + matchList.get(i).getMatchID() + "';\n";
        }
        Statement stmt = Connect.getInstance().getStatement();
        try {
            System.out.println(query);
            stmt.execute(query);
        } catch (SQLException ex) {
            throw new StorageException("could not execute query \n " + query);
        }

    }

    public void setMatches(List<Match> matches, String tournamentName) throws StorageException {
        String query = "";

        String startQuery = "";
        String matchIDQuery = "";
        String matchTeamsQuery = "";

        for (int i = 0; i < matches.size(); i++) {
            try {
                startQuery = "USE TournamentPlannerDB\n"
                        + "INSERT INTO tblMatch (fldTournamentName,fldPoolName) VALUES ('"
                        + tournamentName + "','"
                        + matches.get(i).getTeams().get(0).getPoolName() + "');\n";

                Statement stmt = Connect.getInstance().getStatement();

                stmt.execute(startQuery);

                matchIDQuery = "SELECT MAX(fldMatchID) as matchID FROM tblMatch WHERE fldTournamentName = '" + tournamentName + "'\n";
                ResultSet rs = stmt.executeQuery(matchIDQuery);

                rs.next();
                int matchID = rs.getInt("matchID");

                List<Team> teams = matches.get(i).getTeams();
                for (int j = 0; j < teams.size(); j++) {
                    matchTeamsQuery += "INSERT INTO tblMatchTeams (fldTeamName,fldMatchID) VALUES ('"
                            + teams.get(j).getTeamName() + "',"
                            + matchID + ");\n";
                }
                System.out.println(matchTeamsQuery);
                stmt.execute(matchTeamsQuery);

            } catch (SQLException ex) {
                Logger.getLogger(DB_Match.class.getName()).log(Level.SEVERE, null, ex);
                throw new StorageException("could not store match : " + startQuery + query);
            }
        }
    }

    @Override
    public Object get(String key) throws StorageException {
        String query = "";

        try {
            query = "SELECT * FROM tblMatch WHERE fldTournamentName = '" + key + "';\n";

            Statement stmt = Connect.getInstance().getStatement();

            ResultSet rs = stmt.executeQuery(query);

            List<Match> matchList = new ArrayList();

            while (rs.next()) {

                Match m = new Match();
                m.setStart(rs.getTimestamp("fldStartTime"));
                m.setEnd(rs.getTimestamp("fldEndTime"));

                String teamQuery = "SELECT * FROM tblMatchTeams WHERE fldMatchID = '" + rs.getString("fldMatchID") + "';\n";
                Statement teamStmt = Connect.getInstance().getStatement();

                ResultSet teamRs = teamStmt.executeQuery(teamQuery);

                while (teamRs.next()) {
                    Team t = new Team(teamRs.getString("fldTeamName"));
                    m.addTeam(t);
                }
                matchList.add(m);

            }
            return matchList;

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new StorageException("could not execute query:\n" + query);
        }

    }

    @Override
    public List<StoredObject> getAll() throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    public boolean updateScore(Match m) throws StorageException {
        String query = "";
        try {
            for (int i = 0; i < m.getTeams().size(); i++) {
                query += "UPDATE tblMatchTeams SET fldScore = " + m.getTeams().get(i).getScore() + " WHERE  fldMatchID = '" + m.getMatchID() + "' AND fldTeamName = '" + m.getTeams().get(i).getTeamName() + "';\n";
            }

            Statement stmt = Connect.getInstance().getStatement();
            return stmt.execute(query);
        } catch (SQLException ex) {
            throw new StorageException("could not execute query\n" + query);
        }
    }

    public Map<Team, Integer> getScores(int MatchID) throws StorageException {

        String query = "";
        try {
            query = "SELECT fldTeamName, fldMatchID, fldScore FROM tblMatchTeams WHERE fldMatchID =" + MatchID + ";\n";

            Statement stmt = Connect.getInstance().getStatement();

            ResultSet rs = stmt.executeQuery(query);

            Map<Team, Integer> map = new HashMap();

            while (rs.next()) {
                Team t = new Team(rs.getString("fldTeamName"));
                int i = rs.getInt("fldScore");
                map.put(t, i);

            }

            return map;
        } catch (SQLException ex) {
            throw new StorageException("could not executer statement \n" + query);
        }
    }

}
