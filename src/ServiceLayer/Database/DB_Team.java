/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.Database;

import Domain.Player;
import Domain.Team;
import Domain.TeamManager;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kim Ramgaard
 */
public class DB_Team extends DatabaseAdapter {

    @Override
    public void store(StoredObject so) throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object get(String key) throws StorageException {

        String teamQuery = "";
        List<Team> teams = new ArrayList();
        try {

            teamQuery = "USE TournamentPlannerDB\n"
                    + "\n"
                    + "SELECT  *\n"
                    + "FROM tblTeam AS t\n"
                    + "INNER JOIN tblTeamManager AS m ON t.fldEmail = m.fldEmail\n"
                    + "WHERE fldTournamentName = '" + key + "';";

            Statement stmt = Connect.getInstance().getStatement();
            ResultSet rs = stmt.executeQuery(teamQuery);

            while (rs.next()) {
                Team t = new Team(rs.getString("fldTeamName"));
                TeamManager tm = new TeamManager(rs.getString("fldFirstName"), rs.getString("fldLastName"), rs.getString("fldPhoneNumber"), rs.getString("fldEmail"));
                t.setTeamManager(tm);
                DB_Player PlayerAdapter = new DB_Player();
                t.setPlayers((List<Player>)PlayerAdapter.get(rs.getString("fldTeamName")));
                teams.add(t);
                
                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new StorageException("could not execute query" + teamQuery);
        }
        return teams;
    }

    @Override
    public List<StoredObject> getAll() throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
