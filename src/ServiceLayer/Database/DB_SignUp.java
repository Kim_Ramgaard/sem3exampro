package ServiceLayer.Database;

import Domain.Player;
import Domain.SignUp;
import Domain.Team;
import Domain.TeamManager;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * handle databasecalls regarding SignUp objects
 *
 * @author Kim Ramgaard
 */
public class DB_SignUp extends DatabaseAdapter {

    @Override
    public void store(StoredObject so) throws StorageException {

        try {

            SignUp signup = (SignUp) so.getObject();
            Team t = signup.getTeam();
            TeamManager tm = t.getTeamManager();
            List<Player> players = t.getPlayers();

            String query = "USE TournamentPlannerDB\n"
                    + "			INSERT INTO tblTeamManager(fldFirstName, fldLastName, fldPhoneNumber, fldEmail) VALUES ('" + tm.getFirstName() + "','" + tm.getLastName() + "','" + tm.getPhoneNumber() + "','" + tm.getEmail() + "')\n"
                    + "			INSERT INTO tblTeam(fldTeamName, fldEmail) VALUES ('"+t.getTeamName()+"','"+tm.getEmail()+"')\n";


            for (Player player : players) {
                query += " INSERT INTO tblPlayer(fldFirstName, fldLastName, fldSchoolClass, fldTeamName)"
                        + " VALUES ('" + player.getFirstName() + "','" + player.getLastName() + "','" + player.getSchoolClass() + "','" + t.getTeamName() + "')\n";
            }
            
            System.out.println(query);
            Statement stmt = Connect.getInstance().getStatement();
            stmt.execute(query);

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new StorageException("could not store signup");
        }
    }



    @Override
    public Object get(String key) throws StorageException {
        System.out.println("not implimented" + this.getClass().getName());
        return null;
    }

    @Override
    public List<StoredObject> getAll() throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }




}
