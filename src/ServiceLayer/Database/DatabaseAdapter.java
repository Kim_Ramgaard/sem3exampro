/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.Database;

import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.util.List;

/**
 *
 * @author Kim Ramgaard
 */
public abstract class DatabaseAdapter {
    
    /**
     * store object using storedObject
     * @param so stored object containing the object and the primary key
     */
    public abstract void store(StoredObject so) throws StorageException;
    
    /**
     * get an object via the string key
     * @param key primary key of the table
     * @return 
     */
    public abstract Object get(String key)throws StorageException;
    
    
    
    public abstract List<StoredObject> getAll () throws StorageException; 
    
    
    
    
    
}
