/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.Database;

import Domain.Match;
import ServiceLayer.IServiceHandler;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.util.List;

/**
 *
 * @author Kim Ramgaard
 */
public class DatabaseHandler implements IServiceHandler {

    private DatabaseAdapter[] databaseAdapters;

    public DatabaseHandler() {
        databaseAdapters = new DatabaseAdapter[7];
        databaseAdapters[0] = new DB_Tournament();
        databaseAdapters[1] = new DB_SignUp();
        databaseAdapters[2] = new DB_RuleSet();
        databaseAdapters[3] = new DB_Sport();
        databaseAdapters[4] = new DB_Match();
        databaseAdapters[5] = new DB_Player();
        databaseAdapters[6] = new DB_Team();
        

    }

    @Override
    public void Startup() throws StorageException {
        Connect.getInstance();
    }

    @Override
    public void shutdown() {

    }

    /**
     * store stored object to database
     *
     * @param so object to store
     * @throws ServiceLayer.StorageException
     */
    @Override
    public void store(StoredObject so) throws StorageException {
        String className = so.getKey().split(":")[0].substring(7);
        for (int i = 0; i < databaseAdapters.length; i++) {
            if (databaseAdapters[i].getClass().getName().contains(className)) {
                databaseAdapters[i].store(so);
            }
        }

    }

    @Override
    public StoredObject get(String className, String key) throws StorageException {
        className = className.split(":")[0].substring(7);
        for (int i = 0; i < databaseAdapters.length; i++) {
            if (databaseAdapters[i].getClass().getName().contains(className)) {
                StoredObject so = new StoredObject(databaseAdapters[i].get(key), key, StoredObject.Prioity.MID);
                return so;
            }
        }
        return null;
    }

    public List<StoredObject> getAll(String className) throws StorageException {
        for (int i = 0; i < databaseAdapters.length; i++) {
            if (databaseAdapters[i].getClass().getName().contains(className)) {
                return databaseAdapters[i].getAll();
            }
        }
        throw new StorageException("could not get all "+ className);
    }

        public void setMatches(List<Match> matches, String tournamentName) throws StorageException{
            DB_Match dbMatch = new DB_Match();
            dbMatch.setMatches(matches, tournamentName);
        }

}
