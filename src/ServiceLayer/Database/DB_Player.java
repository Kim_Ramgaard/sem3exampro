package ServiceLayer.Database;

import Domain.Player;
import ServiceLayer.StorageException;
import ServiceLayer.StoredObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * player database adapter
 *
 * @author Kim Ramgaard
 */
public class DB_Player extends DatabaseAdapter {

    @Override
    public void store(StoredObject so) throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object get(String key) throws StorageException {

        String query = "";
        List<Player> players = new ArrayList<>();

        try {
            query = "SELECT * \n"
                    + "FROM tblPlayer\n"
                    + "\n"
                    + "WHERE\n"
                    + "fldTeamName = '" + key + "'\n";

            Statement stmt = Connect.getInstance().getStatement();

            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                Player p = new Player(rs.getString("fldFirstName"), rs.getString("fldLastName"), rs.getString("fldSchoolClass"));
                players.add(p);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new StorageException("could not execute query"+query);
        }
        
        return players;

    }

    @Override
    public List<StoredObject> getAll() throws StorageException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
