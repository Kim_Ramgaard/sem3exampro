/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.Database;

import Domain.Tournament;
import ServiceLayer.ServiceLayerHandler;
import ServiceLayer.StorageException;

/**
 *
 * @author Kim Ramgaard
 */
public class DatabaseTEST {

    public static void main(String[] args) throws StorageException {
        Tournament t  = (Tournament)ServiceLayerHandler.getInstance().get(Tournament.class.getName(), "Awesome Tournament");
        
        System.out.println(t.getName());
    }

}
