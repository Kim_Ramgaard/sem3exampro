package ServiceLayer;

import Domain.Match;
import Domain.PoolMatch;
import Domain.Team;
import java.util.Map;

/**
 * used for testing the serviceLayer
 *
 * @author Kim Ramgaard
 */
public class ServiceLayerTEST {

    public static void main(String[] args) throws StorageException {
         ServiceLayerHandler.getInstance().setMatches(null, null);
    }
}
