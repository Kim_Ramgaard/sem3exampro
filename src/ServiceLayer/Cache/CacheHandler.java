package ServiceLayer.Cache;

import ServiceLayer.IServiceHandler;
import ServiceLayer.StoredObject;

/**
 * handle methodcalls to the cachepackage
 *
 * @author Kim Ramgaard
 */
public class CacheHandler implements IServiceHandler{


    @Override
    public void Startup() {
        
    }
    
    @Override
    public void shutdown() {
        CacheCleaner.setRun(false);
    }
    
    @Override
    public void store(StoredObject so) {
        Cache.getInstance().store(so);
    }

    @Override
    public StoredObject get(String className, String key){
        return Cache.getInstance().get(className, key);
    }


    
}
