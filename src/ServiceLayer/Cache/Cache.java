package ServiceLayer.Cache;

import ServiceLayer.StoredObject;
import ServiceLayer.PropertyAdapter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kim Ramgaard
 */
public class Cache {

    private Map<String, StoredObject> cacheMap;

    private Cache() {
        //get inforamtion about size and load factor from propertyile and then set map (default = 1000,100)
        Properties cacheInfo = PropertyAdapter.getInstance().getProperties();
        int size = Integer.parseInt(cacheInfo.getProperty("Cache_Size", "1000"));
        int loadFactor = Integer.parseInt(cacheInfo.getProperty("Cache_LoadFactor", "100"));
        cacheMap = new HashMap<>(size, loadFactor);
        
        //starts cacheGarbage collector
        Thread CacheCleaner = new Thread(new CacheCleaner());
        CacheCleaner.setPriority(Thread.MIN_PRIORITY);
        CacheCleaner.start();
    }

    public Map<String, StoredObject> getCacheMap() {
        return cacheMap;
    }

    /**
     * search in CacheMap, if not found then look up in harddrive
     *
     * @param className full name of the class (ex String.getClass.getName)
     * @param key primary key
     * @return the object that was stored under the specific key.
     */
    public StoredObject get(String className, String key) {
        StoredObject c;
        String fullKey = className + ":" + key;
        c = cacheMap.get(fullKey);
        if (c == null) {
            Logger.getLogger(Cache.class.getName()).log(Level.FINE, "searched for {0} in cache and was not found", fullKey);
        }
        return c;
    }

    /**
     * check for old values in the cache and delete them if time to live have expired
     * @return the number of entries deleted
     */
    protected synchronized int checkCache() {

        Iterator it = cacheMap.entrySet().iterator();

        Date d = new Date();
        int count = 0;

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry<String, StoredObject>) it.next();
            StoredObject so = (StoredObject) pair.getValue();
            if (so.getTimeToLive().after(d)) {
                it.remove();
                count++;
            }
        }

        return count;
    }

    public void store(StoredObject obj) {
        cacheMap.put(obj.getKey(), obj);
        
    }
    

    public static Cache getInstance() {
        return CacheHolder.INSTANCE;
    }

    private static class CacheHolder {

        private static final Cache INSTANCE = new Cache();

    }
}
