/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer.Cache;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kim Ramgaard
 */
public class CacheCleaner implements Runnable {
    
    private static boolean run = true;

    public static void setRun(boolean run) {
        CacheCleaner.run = run;
    }

 
    
    

    @Override
    public void run() {
        while (run) {
              try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(CacheCleaner.class.getName()).log(Level.SEVERE, "cachecleaner timer intercepted", ex);
            }
            int i = Cache.getInstance().checkCache();
                Logger.getLogger(CacheCleaner.class.getName()).log(Level.FINE, "number of deleted entries in cache: {0}", i);
           
              
        }
    }

}
