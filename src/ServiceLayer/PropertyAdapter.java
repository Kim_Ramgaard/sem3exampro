/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceLayer;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * singletonclass to get the property config file
 * @author Kim Ramgaard
 */
public class PropertyAdapter {
    
    private Properties properties;
    
    
    
    private PropertyAdapter() {
        properties = new Properties();
        try {
            properties.load(new FileInputStream("src/config.properties"));
        } catch (IOException ex) {
            Logger.getLogger(PropertyAdapter.class.getName()).log(Level.SEVERE, "cannot get connection to config file", ex);
        }
        
    }

    public Properties getProperties() {
        return properties;
    }
    
    
    
    public static PropertyAdapter getInstance() {
        return PropertyAdapterHolder.INSTANCE;
    }
    
    private static class PropertyAdapterHolder {

        private static final PropertyAdapter INSTANCE = new PropertyAdapter();
    }
}
