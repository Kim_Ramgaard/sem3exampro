/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import Domain.PresentationHandler;
import Domain.Tournament;
import ServiceLayer.StorageException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Niels
 */
public class ChoosingTournamentPanel extends JPanel implements Observer {

    private JLabel textInfo;
    private JComboBox tournamentsBox;
    private List<Tournament> tournaments;
    private JTextArea infoArea;

    public ChoosingTournamentPanel() {
        MakeTournament.registerObserver(this);

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        infoArea = new JTextArea();

        tournamentsBox = new JComboBox();

        try {
            tournaments = new ArrayList<>(PresentationHandler.getInstance().getTournaments());
        } catch (StorageException ex) {
            Logger.getLogger(MakeTournament.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        for (Iterator<Tournament> iterator = tournaments.iterator(); iterator.hasNext();) {
            tournamentsBox.addItem(iterator.next().getName());
        }

        textInfo = new JLabel("Choose the tournament your team want to take part of:");

        ComboBoxAction ca = new ComboBoxAction();
        tournamentsBox.addActionListener(ca);
        gc.gridx = 0;
        gc.gridy = 0;
        add(textInfo, gc);
        gc.gridy = 1;
        add(tournamentsBox, gc);
        gc.gridy = 2;
        add(infoArea, gc);
    }

    @Override
    public void update(Tournament t) {
        tournaments.add(t);
        tournamentsBox.addItem(t.getName());
    }

    public class ComboBoxAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < tournaments.size(); i++) {
                if ((String) tournamentsBox.getSelectedItem() == tournaments.get(i).getName()) {
                    infoArea.setText("Sport: " + tournaments.get(i).getSport()
                            + "\nStart date: " + tournaments.get(i).getStartDate()
                            + "\nType of ruleset: " + tournaments.get(i).getRuleName()
                            + "\nRuleset description: " + tournaments.get(i).getRuleDescription()
                            + "\nSignupFee: " + tournaments.get(i).getSignupfee());
                }
            }
        }
    }

    //returns the selected tournament in the combox 
    public Tournament getSelectedTournament() {
        int i = tournamentsBox.getSelectedIndex();
        return tournaments.get(i);
    }

}
