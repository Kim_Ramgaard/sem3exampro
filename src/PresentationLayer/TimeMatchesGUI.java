/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import Domain.Match;
import Domain.PresentationHandler;
import Domain.Team;
import Domain.Tournament;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.*;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Niels
 */
public class TimeMatchesGUI extends JPanel {

    private JLabel tournamentNameLabel, startDateLabel, endDateLabel, team1Label, team2Label, dateLabel, startTimeLabel, endTimeLabel, durationLabel, vsLabel;
    private List<JSpinner> startTimeList;
    private List<JSpinner> endTimeList;
    private List<Match> matches;
    private List<Team> team1, team2;
    private List<JXDatePicker> date;
    private Tournament tournament;
    private List<JLabel> team1Labels, team2Labels;
    private JPanel buttonPanel, mainPanal;
    private JButton confirmButton, cancelButton;

    public TimeMatchesGUI(Tournament t) {
        tournament = t;
        buttonPanel = new JPanel();
        mainPanal = new JPanel();
        mainPanal.setLayout(new GridBagLayout());
        setLayout(new BorderLayout());
        GridBagConstraints gc = new GridBagConstraints();
        startTimeList = new ArrayList<>();
        endTimeList = new ArrayList<>();
        team1Labels = new ArrayList<>();
        team2Labels = new ArrayList<>();
        date = new ArrayList<>();
        team1 = new ArrayList<>();
        team2 = new ArrayList<>();
        matches = new ArrayList<>();
        confirmButton = new JButton("Confirm");
        cancelButton = new JButton("Cancel");

        tournamentNameLabel = new JLabel("Tournament: " + tournament.getName());
        startDateLabel = new JLabel("Start date: " + tournament.getStartDate());
        endDateLabel = new JLabel("End date: " + tournament.getEndDate());
        team1Label = new JLabel("Team 1");
        vsLabel = new JLabel("vs.");
        team2Label = new JLabel("Team 2");
        dateLabel = new JLabel("Date");
        startTimeLabel = new JLabel("Start time");
        endTimeLabel = new JLabel("End time");

        gc.insets = new Insets(0, 0, 10, 10);
        gc.gridx = 0;
        gc.gridy = 0;
        gc.gridwidth = 2;
        mainPanal.add(tournamentNameLabel, gc);
        gc.gridx += 2;
        mainPanal.add(startDateLabel, gc);
        gc.gridx += 2;
        mainPanal.add(endDateLabel, gc);
        gc.gridwidth = 1;
        gc.gridx = 0;
        gc.gridy++;
        mainPanal.add(team1Label, gc);
        gc.gridx++;
        mainPanal.add(vsLabel, gc);
        gc.gridx++;
        mainPanal.add(team2Label, gc);
        gc.gridx++;
        mainPanal.add(dateLabel, gc);
        gc.gridx++;
        mainPanal.add(startTimeLabel, gc);
        gc.gridx++;
        mainPanal.add(endTimeLabel, gc);

        generateArrays();

        gc.gridy++;

        for (int i = 0; i < date.size(); i++) {
            gc.gridx = 0;
            mainPanal.add(team1Labels.get(i), gc);
            gc.gridx += 2;
            mainPanal.add(team2Labels.get(i), gc);
            gc.gridx++;
            mainPanal.add(date.get(i), gc);
            gc.gridx++;
            mainPanal.add(startTimeList.get(i), gc);
            gc.gridx++;
            mainPanal.add(endTimeList.get(i), gc);
            gc.gridy++;
        }
        buttonPanel.add(cancelButton);
        buttonPanel.add(confirmButton);
        add(buttonPanel, BorderLayout.SOUTH);
        add(mainPanal, BorderLayout.CENTER);

        ButtonHandler bh = new ButtonHandler();
        cancelButton.addActionListener(bh);
        confirmButton.addActionListener(bh);

    }

    public class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == confirmButton) {
                saveTimes();
                
            } else if (e.getSource() == cancelButton) {
                TournamentMatchHolder.swap("select");
            }
        }
    }

    public void saveTimes() {
        Calendar cal = Calendar.getInstance();

        for (int i = 0; i < matches.size(); i++) {
            Date startTime = (Date) startTimeList.get(i).getValue();
            Date endTime = (Date) startTimeList.get(i).getValue();
            cal.set(Calendar.MONTH, date.get(i).getDate().getMonth());
            cal.set(Calendar.YEAR, date.get(i).getDate().getYear() + 1900);
            cal.set(Calendar.DATE, date.get(i).getDate().getDate());
            cal.set(Calendar.MINUTE, startTime.getMinutes());
            cal.set(Calendar.HOUR, startTime.getHours());
            Date first = cal.getTime();

            cal.set(Calendar.MINUTE, endTime.getMinutes());
            cal.set(Calendar.HOUR, endTime.getMinutes());
            Date second = cal.getTime();

            matches.get(i).setStart(first);
            matches.get(i).setEnd(second);

        }
        PresentationHandler.getInstance().StoreTimeMatches(matches);

        //System.out.println(da);
    }

    public void generateArrays() {
        for (int i = 0; i < tournament.getMatches().size(); i++) {
            matches.add(tournament.getMatches().get(i));
            team1.add(matches.get(i).getTeams().get(0));
            team2.add(matches.get(i).getTeams().get(1));

            team1Labels.add(new JLabel(team1.get(i).getTeamName()));
            team2Labels.add(new JLabel(team2.get(i).getTeamName()));

            SpinnerModel m1 = new SpinnerDateModel();
            SpinnerModel m2 = new SpinnerDateModel();

            startTimeList.add(new JSpinner(m1));
            endTimeList.add(new JSpinner(m2));
            JComponent editor = new JSpinner.DateEditor(startTimeList.get(i), "HH:mm");
            JComponent editor1 = new JSpinner.DateEditor(endTimeList.get(i), "HH:mm");

            startTimeList.get(i).setEditor(editor);
            endTimeList.get(i).setEditor(editor1);
            date.add(new JXDatePicker()); //set date by getting it from the tournament 

        }
    }

}
