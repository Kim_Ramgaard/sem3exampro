/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import Domain.PresentationHandler;
import Domain.Tournament;
import ServiceLayer.StorageException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Niels
 */
public class MakeTournament extends JPanel {

    private Tournament t;
    private static List<Observer> observers;
    private JButton createTournament;
    private JLabel choosesportLabel, rulesetLabel, tournamentNameLabel, startDateLabel, endDateLabel, feeLabel;
    private JComboBox sportGames, ruleSet;
    private JTextField tournamentNameTF, feeTF;
    private JXDatePicker startDate, endDate;

    public MakeTournament() {

        observers = new ArrayList<Observer>();
        createTournament = new JButton("Create tournament");
        choosesportLabel = new JLabel("Choose the sport for the tournament:");
        rulesetLabel = new JLabel("Choose the ruleset for the tournament:");
        tournamentNameLabel = new JLabel("Write a name for the tournament:");
        startDateLabel = new JLabel("When is the tournament taking place?");
        endDateLabel = new JLabel("When is the tournament over?");
        feeLabel = new JLabel("What should the fee for the sign up be?");
        startDate = new JXDatePicker();
        endDate = new JXDatePicker();

        List<String> list;
        try {
            list = new ArrayList<>(PresentationHandler.getInstance().getSportnames());
            sportGames = new JComboBox(list.toArray());
        } catch (StorageException ex) {
            Logger.getLogger(MakeTournament.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<String> rList;
        try {
            rList = new ArrayList<>(PresentationHandler.getInstance().getRuleSets());
            ruleSet = new JComboBox(rList.toArray());
        } catch (StorageException ex) {
            Logger.getLogger(MakeTournament.class.getName()).log(Level.SEVERE, null, ex);
        }

        tournamentNameTF = new JTextField(20);
        feeTF = new JTextField(5);

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = new Insets(0, 0, 5, 10);
        gc.gridx = 0;
        gc.gridy = 0;
        add(choosesportLabel, gc);
        gc.gridy = 1;
        add(sportGames, gc);
        gc.gridy = 2;
        add(tournamentNameLabel, gc);
        gc.gridy = 3;
        add(tournamentNameTF, gc);
        gc.gridy = 4;
        add(startDateLabel, gc);
        gc.gridy = 0;
        gc.gridx = 1;
        add(rulesetLabel, gc);
        gc.gridy = 1;
        gc.gridx = 1;
        add(ruleSet, gc);
        gc.gridy = 2;
        gc.gridx = 1;
        add(feeLabel, gc);
        gc.gridy = 3;
        gc.gridx = 1;
        add(feeTF, gc);
        gc.gridy = 4;
        gc.gridx = 1;
        add(endDateLabel, gc);
        gc.gridy = 5;
        gc.gridx = 0;
        add(startDate, gc);
        gc.gridy = 5;
        gc.gridx = 1;
        add(endDate, gc);
        gc.gridy = 6;
        gc.gridx = 0;
        gc.gridwidth = 2;
        gc.anchor = GridBagConstraints.CENTER;
        add(createTournament, gc);

        ButtonHandler bh = new ButtonHandler();
        createTournament.addActionListener(bh);
    }

    public class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            makeTournament();
            notifyObservers();
        }
    }

    public void makeTournament() {
         t = new Tournament(tournamentNameTF.getText(), sportGames.getSelectedItem().toString(),
                Double.parseDouble(feeTF.getText()), startDate.getDate(), endDate.getDate(), null, ruleSet.getSelectedItem().toString(), null);
        PresentationHandler.getInstance().storeTournament(t);

      
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, "You have now created a tournament with the name of: " + tournamentNameTF.getText());
        tournamentNameTF.setText("");
        startDate.setDate(null);
        endDate.setDate(null);
        feeTF.setText("");
    }

    public static void registerObserver(Observer o) {
        observers.add(o);
    }


    public void notifyObservers() {
        for (int i = 0; i < observers.size(); i++) {
            observers.get(i).update(t);
            
        }
    }
}
