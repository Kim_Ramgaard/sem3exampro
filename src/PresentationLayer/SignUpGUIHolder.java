/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Niels
 */
public class SignUpGUIHolder extends JPanel {

    private static JPanel panelHolder;
    private JButton next, cancel;
    private JPanel mainPanel, buttonPanel;
    private static CardLayout cards = new CardLayout();
    private ChoosingTournamentPanel choose = new ChoosingTournamentPanel();

    public SignUpGUIHolder() {
       
        mainPanel = new JPanel();
        buttonPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        next = new JButton("Next");
        cancel = new JButton("Cancel");
        buttonPanel.add(cancel);
        buttonPanel.add(next);

        panelHolder = new JPanel(cards);

        mainPanel.add(choose, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        panelHolder.add(mainPanel, "Info");

        add(panelHolder);

        ButtonHandler bh = new ButtonHandler();
        next.addActionListener(bh);
        cancel.addActionListener(bh);      
    }

    public class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == next) {
                //create a new panel 
                SignUpTeamGUI signUpTeamGUI = new SignUpTeamGUI(choose.getSelectedTournament());
                //add it the panel                 
                panelHolder.add(signUpTeamGUI, "signup");
                //make the cardlayout show the panel 
                cards.show(panelHolder, "signup");
            } else if (e.getSource() == cancel) {
                //dispose();
            }
        }
    }
    //made static so it is useable from the otherpanel so we get back to the other panel 
    public static void swapView(String key) {
        cards.show(panelHolder, key);
    }


}
