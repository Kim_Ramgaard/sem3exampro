/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import Domain.PresentationHandler;
import Domain.Tournament;
import Domain.TournamentException;
import ServiceLayer.StorageException;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author Niels
 */
public class TournamentMatchHolder extends JPanel implements Observer {

    private JPanel selectTournamentPanel;
    private static JPanel cards;
    private TimeMatchesGUI timeMatchesGUI;
    private String[] nameOfTournaments;
    private List<Tournament> tournaments;
    private JComboBox combobox;
    private static CardLayout cardLayout = new CardLayout();
    private JButton nextButton;
    private JTextArea infoArea;
    private JFrame errorFrame;

    public TournamentMatchHolder() {
        MakeTournament.registerObserver(this);
        selectTournamentPanel = new JPanel();
        selectTournamentPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        cards = new JPanel();
        cards.setLayout(cardLayout);
        nextButton = new JButton("Next");
        combobox = new JComboBox();
        infoArea = new JTextArea();

        try {
            tournaments = new ArrayList<>(PresentationHandler.getInstance().getTournaments());
        } catch (StorageException ex) {
            Logger.getLogger(TournamentMatchHolder.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Iterator<Tournament> iterator = tournaments.iterator(); iterator.hasNext();) {
            combobox.addItem(iterator.next().getName());
        }

        gc.gridx = 0;
        gc.gridy = 0;
        gc.insets = new Insets(0, 0, 5, 5);
        selectTournamentPanel.add(combobox, gc);
        gc.gridy++;
        selectTournamentPanel.add(infoArea, gc);
        gc.gridy++;
        selectTournamentPanel.add(nextButton, gc);
    
        cards.add(selectTournamentPanel, "select");
        add(cards);

        ButtonHandler bh = new ButtonHandler();
        nextButton.addActionListener(bh);
        combobox.addActionListener(bh);

    }

    @Override
    public void update(Tournament t) {
        tournaments.add(t);
        combobox.addItem(t.getName());

    }

    public class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == nextButton) {
                createNewPanel();
            } else if (e.getSource() == combobox) {
                for (int i = 0; i < tournaments.size(); i++) {
                    if ((String) combobox.getSelectedItem() == tournaments.get(i).getName()) {
                        infoArea.setText("Sport: " + tournaments.get(i).getSport()
                                + "\nStart date: " + tournaments.get(i).getStartDate()
                                + "\nType of ruleset: " + tournaments.get(i).getRuleName()
                                + "\nRuleset description: " + tournaments.get(i).getRuleDescription()
                                + "\nSignupFee: " + tournaments.get(i).getSignupfee());
                    }
                }
            }

        }
    }

    public void createNewPanel() {
        
        String tName = (String) combobox.getSelectedItem();
        Tournament t;
        try {
            t = PresentationHandler.getInstance().getTournament(tName);
            t.setTeams(PresentationHandler.getInstance().getTeams(t.getName()));
            t.makeMatches();
            TimeMatchesGUI timeMatchesGUI = new TimeMatchesGUI(t);
            cards.add(timeMatchesGUI);
            cards.add(timeMatchesGUI, "match");
            cardLayout.show(cards, "match");
        } catch (StorageException ex) {
            JOptionPane.showMessageDialog(errorFrame,ex.getMessage());
        } catch (TournamentException ex) {
            JOptionPane.showMessageDialog(errorFrame,ex.getMessage());
        }
    }

    public static void swap(String key) {
        cardLayout.show(cards, key);
    }

}
