package PresentationLayer;

import Domain.Tournament;

/**
 *
 * @author Nour
 */
public interface Observer 
{
    public void update(Tournament t);
}
