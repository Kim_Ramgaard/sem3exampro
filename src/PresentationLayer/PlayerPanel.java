/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author Niels
 */
public class PlayerPanel extends JPanel {

    private JLabel firstNameLabel, lastNameLabel, SchoolClassLabel;
    private JTextField firstNameTF, lastNameTF, SchoolClassTF;

    public PlayerPanel() {
        firstNameLabel = new JLabel("First name: ");
        lastNameLabel = new JLabel("Last name: ");
        SchoolClassLabel = new JLabel("School class: ");
        firstNameTF = new JTextField(10);
        lastNameTF = new JTextField(10);
        SchoolClassTF = new JTextField(5);
        
        
        setLayout(new GridLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.anchor = GridBagConstraints.WEST;
        gc.gridx = 0;
        add(firstNameLabel,gc);
        gc.gridx = 1;
        add(firstNameTF,gc);
        gc.gridx = 2;
        add(lastNameLabel,gc);
        gc.gridx = 3;
        add(lastNameTF,gc);
        gc.gridx = 4;
        add(SchoolClassLabel,gc);
        gc.gridx = 5;
        add(SchoolClassTF,gc);
    
    }

    public void removeThisPlayer(){
        removeAll(); 
    }

    public String getFirstNameTF() {
        return firstNameTF.getText();
    }

    public String getLastNameTF() {
        return lastNameTF.getText();
    }

    public String getSchoolClassTF() {
        return SchoolClassTF.getText();
    }

    public void setFirstNameTF(JTextField firstNameTF) {
        this.firstNameTF = firstNameTF;
    }

    public void setLastNameTF(JTextField lastNameTF) {
        this.lastNameTF = lastNameTF;
    }

    public void setSchoolClassTF(JTextField SchoolClassTF) {
        this.SchoolClassTF = SchoolClassTF;
    }
    
    
    
}
