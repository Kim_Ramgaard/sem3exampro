/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import Domain.Player;
import Domain.PresentationHandler;
import Domain.SignUp;
import Domain.Team;
import Domain.TeamManager;
import Domain.Tournament;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author Niels
 */
public class SignUpTeamGUI extends JPanel {

    private JButton addPlayer, cancelButton, goToPaymentButton;
    private JLabel fName, lName, phoneNumber, teamManagerLabel, playerLabel, teamNameLabel, emailLabel;
    private JTextField managerFNameTF, managerLNameTF, phoneNumberTF, teamNameTF, emailTF;
    private JPanel buttomPanel, mainPanel;
    private static GridBagConstraints gc = new GridBagConstraints();
    private static int gy;
    private List<PlayerPanel> players = new ArrayList<>();
    private List<JButton> removeButtons = new ArrayList<>();
    private ButtonHandler bh = new ButtonHandler();
    private Tournament t;

    public SignUpTeamGUI(Tournament tournament) {
        t = tournament;
        setLayout(new BorderLayout());
        GridBagConstraints gc = new GridBagConstraints();
        cancelButton = new JButton("Cancel");
        goToPaymentButton = new JButton("Continue to payment");
        buttomPanel = new JPanel();
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        addPlayer = new JButton("+Add player");
        fName = new JLabel("First name: ");
        lName = new JLabel("Last name");
        phoneNumber = new JLabel("Phonenumber: ");
        teamManagerLabel = new JLabel("Team manager");
        managerFNameTF = new JTextField(10);
        managerLNameTF = new JTextField(10);
        phoneNumberTF = new JTextField(10);
        playerLabel = new JLabel("Players");
        teamNameTF = new JTextField(10);
        teamNameLabel = new JLabel("The name of your team: ");
        emailLabel = new JLabel("Email:");
        emailTF = new JTextField(10);

        gc.anchor = GridBagConstraints.WEST;
        gc.gridx = 0;
        mainPanel.add(teamManagerLabel, gc);
        gc.insets = new Insets(0, 0, 5, 5);
        gc.gridy = 1;
        mainPanel.add(fName, gc);
        gc.gridx++;
        mainPanel.add(managerFNameTF, gc);
        gc.gridy++;
        gc.gridx--;
        mainPanel.add(lName, gc);
        gc.gridx++;

        mainPanel.add(managerLNameTF, gc);
        gc.gridx--;
        gc.gridy++;
        mainPanel.add(phoneNumber, gc);
        gc.gridx++;
        mainPanel.add(phoneNumberTF, gc);
        gc.gridx = 0;
        gc.gridy++;
        mainPanel.add(emailLabel, gc);
        gc.gridx++;
        mainPanel.add(emailTF, gc);
        gc.gridy++;
        gc.gridx--;
        mainPanel.add(teamNameLabel, gc);
        gc.gridx++;
        mainPanel.add(teamNameTF, gc);
        gc.insets = new Insets(40, 0, 5, 0);
        gc.gridx = 0;
        gc.gridy++;
        mainPanel.add(playerLabel, gc);
        gc.insets = new Insets(0, 0, 5, 0);

        for (int i = 0; i < 4; i++) {
            players.add(new PlayerPanel());
            removeButtons.add(new JButton("Remove"));
        }

        for (int i = 0; i < players.size(); i++) {
            gc.gridwidth = 6;
            gc.gridy++;
            gc.gridx = 0;
            mainPanel.add(players.get(i), gc);
            gc.gridx = 7;
            gc.gridwidth = 1;
            mainPanel.add(removeButtons.get(i), gc);

        }
        gy = gc.gridy;
        gy++;

        buttomPanel.add(addPlayer);
        buttomPanel.add(cancelButton);
        buttomPanel.add(goToPaymentButton);
        addPlayer.addActionListener(bh);
        cancelButton.addActionListener(bh);
        goToPaymentButton.addActionListener(bh);
        for (JButton removeButton : removeButtons) {
            removeButton.addActionListener(bh);
        }

        add(buttomPanel, BorderLayout.SOUTH);
        add(mainPanel, BorderLayout.CENTER);

    }

    public class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addPlayer) {
                gc.gridy = gy++;
                addPlayer();
                revalidate();
            } else if (e.getSource() == goToPaymentButton) {
                registerTeam();               

            } else if (e.getSource() == cancelButton) {
                SignUpGUIHolder.swapView("Info");
            }
            for (int i = 0; i < removeButtons.size(); i++) {
                if (e.getSource() == removeButtons.get(i)) {
                    removePlayer(i);
                }
            }
        }
    }

    //add players to the GUI 
    public void addPlayer() {
        players.add(new PlayerPanel());
        JButton b1 = new JButton("Remove");
        b1.addActionListener(bh);
        removeButtons.add(b1);
        gc.insets = new Insets(0, 0, 5, 0);
        gc.gridwidth = 6;
        gc.gridx = 0;
        mainPanel.add(players.get(players.size() - 1), gc);
        gc.gridwidth = 1;
        gc.gridx = 7;
        mainPanel.add(removeButtons.get(removeButtons.size() - 1), gc);
        revalidate();
    }

    //Shit aint removing the buttons from the panel.. 
    //gets arrayout out of bound, and cant remove when pressing on all buttons.. 
    public void removePlayer(int i) {
        players.remove(i).removeThisPlayer();
        players.remove(i);
        removeButtons.remove(i).removeAll();
        removeButtons.remove(i);
        revalidate();
    }

    //this will register the whole team to the database 
    public void registerTeam() {
        List<Player> playerDomain = new ArrayList();
        for (int i = 0; i < players.size(); i++) {
            Player p = new Player(players.get(i).getFirstNameTF(), players.get(i).getLastNameTF(), players.get(i).getSchoolClassTF());
            playerDomain.add(p);
        }
        TeamManager tm = new TeamManager(managerFNameTF.getText(), managerLNameTF.getText(), phoneNumberTF.getText(), emailTF.getText());
        Team team = new Team(teamNameTF.getText(), playerDomain, tm);
        SignUp signUp = new SignUp(team);
        PresentationHandler.getInstance().storeSignup(signUp);
        //PresentationHandler.getInstance().storeTournament(t);
        JFrame frame = new JFrame();
        //pop up dialog 
        JOptionPane.showMessageDialog(frame, "Your team has now been added to the tournament as: "+teamNameTF.getText());
        //switch back to previous panel
        SignUpGUIHolder.swapView("Info");
    }   
}
