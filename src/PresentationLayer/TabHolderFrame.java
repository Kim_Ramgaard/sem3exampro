/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PresentationLayer;

import ServiceLayer.ServiceLayerHandler;
import ServiceLayer.StorageException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 *
 * @author Niels
 */
public class TabHolderFrame extends JFrame {
    
    public TabHolderFrame (){
        
          try {
            ServiceLayerHandler.getInstance().startup();
        } catch (StorageException ex) {
            Logger.getLogger(MakeTournament.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JTabbedPane tabbedPane = new JTabbedPane(); 
        
        JComponent makeTournament = new MakeTournament(); 
        JComponent signUp = new SignUpGUIHolder();
        JComponent tournamentTime = new TournamentMatchHolder();
        
        tabbedPane.addTab("Make tournament", makeTournament);
        tabbedPane.addTab("Sign up", signUp);
        tabbedPane.addTab("Set time on matches", tournamentTime);
        
        
        add(tabbedPane);
        
        setSize(800,500);
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        TabHolderFrame at = new TabHolderFrame();
        at.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
}
