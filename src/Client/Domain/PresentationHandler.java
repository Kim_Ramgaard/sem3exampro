package Client.Domain;

import Client.RMIClient.RMIClientHandler;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Kim Ramgaard
 */
public class PresentationHandler {

    private PresentationHandler() {
    }

    public static PresentationHandler getInstance() {
        return PresentationHandlerHolder.INSTANCE;
    }

    public List<Match> getMatches(String tournamentName) {
        List<Match> matches = RMIClientHandler.getInstance().getMatches(tournamentName);
        return matches;
    }

    public Map<Team, Integer> getScores(int matchID) {
        Map<Team, Integer> scores = RMIClientHandler.getInstance().getScores(matchID);
        return scores;
    }

    public List<Tournament> getTournaments() {
        List<Tournament> tList = RMIClientHandler.getInstance().getTournaments();
        return tList;
    }

    private static class PresentationHandlerHolder {

        private static final PresentationHandler INSTANCE = new PresentationHandler();
    }

}
