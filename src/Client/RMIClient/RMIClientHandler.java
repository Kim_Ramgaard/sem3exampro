package Client.RMIClient;

import Domain.Match;
import Domain.Team;
import Domain.Tournament;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Kim Ramgaard
 */
public class RMIClientHandler {
    
    private static RMIClientHandler instance;
    private RMIClient client;
    
    private RMIClientHandler() {
        client = RMIClient.getInstance();
    }
    
    public static RMIClientHandler getInstance(){
        if(instance == null){
            instance = new RMIClientHandler();
        }
        return instance;
    }
    
    public List<Match> getMatches(String tournamentName){
        List<Match> matches = client.getMatches(tournamentName);
        return matches;
    }
    public Map<Team, Integer> getScores(int matchID){
        Map<Team, Integer> scores = client.getScores(matchID);
        return scores;
    }
    public List<Tournament> getTournaments(){
        List<Tournament> tList = client.getTournaments();
        return tList;
    }
    
    public void startupClient(){
        RMIClient.getInstance();
    }
    
    

}
