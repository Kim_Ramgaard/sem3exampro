/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client.RMIClient;

import Domain.Match;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Domain.RMIServer.IScoreBoard;
import Domain.Team;
import Domain.Tournament;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author Niels
 */
public class RMIClient {

    private static RMIClient instance;
    private IScoreBoard remote;
    private Properties properties;

    private RMIClient() {
        try {
            properties = new Properties();
            properties.load(new FileInputStream("src/RMIConfig.properties"));
            String address = properties.getProperty("HOST_IP");
            String port = properties.getProperty("HOST_PORT");
            remote = (IScoreBoard) Naming.lookup("rmi://"+address+":"+port+"/TournamentService");
            System.out.println("Connected to server...");

        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static RMIClient getInstance() {
        if (instance == null) {
            instance = new RMIClient();
        }
        return instance;
    }

    public List<Match> getMatches(String tournamentName) {
        List<Match> matches = new ArrayList<>();
        try {
            matches = remote.getMatches(tournamentName);
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matches;
    }

    public Map<Team, Integer> getScores(int matchID) {
        Map<Team, Integer> scores = new HashMap<>();
        try {
            scores = remote.getScores(matchID);
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scores;
    }
    
    public List<Tournament> getTournaments(){
        List<Tournament> tList = new ArrayList<>();
        try {
            tList = remote.getTournaments();
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tList;
    }

}
