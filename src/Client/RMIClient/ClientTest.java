/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client.RMIClient;

import Domain.Match;
import Domain.Team;
import Domain.Tournament;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rasmus
 */
public class ClientTest {
    public static void main(String[] args) {
        RMIClient client = RMIClient.getInstance();
        List<Match> matches = client.getMatches("Awesome Tournament");
        List<Tournament> tournaments = client.getTournaments();
        Map<Team, Integer> scores = client.getScores(2);
        Iterator it = scores.entrySet().iterator();
        for (int i = 0; i < matches.size(); i++) {
            System.out.println(matches.get(i).toString());
        }
        while(it.hasNext()){
            Map.Entry pairs = (Map.Entry) it.next();
            System.out.println(pairs.getKey() +" = " + pairs.getValue());
            it.remove();
        }
        for (int i = 0; i < tournaments.size(); i++) {
            System.out.println(tournaments.get(i));            
        }

    }
}
