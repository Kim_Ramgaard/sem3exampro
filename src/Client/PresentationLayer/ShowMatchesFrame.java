/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client.PresentationLayer;

import Client.Domain.Match;
import Client.Domain.PresentationHandler;
import Client.Domain.Team;
import Client.Domain.Tournament;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.*;

/**
 *
 * @author Niels
 */
public class ShowMatchesFrame extends JFrame {

    private List<Team> team1;
    private List<Team> team2;
    private JComboBox combobox;
    private List<Tournament> tournaments;
    private JPanel mainPanel, topPanel;
    private List<Match> match;
    private List<MatchPanel> matchPanels;

    public ShowMatchesFrame() {
        setTitle("Matches");
        mainPanel = new JPanel();
        topPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        setLayout(new BorderLayout());
        GridBagConstraints gc = new GridBagConstraints();
        combobox = new JComboBox();
        matchPanels = new ArrayList<>();
        match = new ArrayList<>();

        tournaments = new ArrayList<>(); 
        tournaments = (List)PresentationHandler.getInstance().getTournaments();
        for (int i = 0; i < tournaments.size(); i++) {
            combobox.addItem(tournaments.get(i).getName());
            
        }
        
//        for (Iterator<Tournament> iterator = tournaments.iterator(); iterator.hasNext();) {
//            combobox.addItem(iterator.next().getName());
//        }

        showMatch();
        gc.gridx = 0;
        gc.gridy = 0;
        gc.insets = new Insets(0, 0, 5, 5);

        for (int i = 0; i < matchPanels.size(); i++) {
            mainPanel.add(matchPanels.get(i).getTeam1(), gc);
            gc.gridx++;
            mainPanel.add(matchPanels.get(i).getSore1(), gc);
            gc.gridx++;
            mainPanel.add(matchPanels.get(i).getScore2(), gc);
            gc.gridx++;
            mainPanel.add(matchPanels.get(i).getTeam2(), gc);
            gc.gridx++;
            mainPanel.add(matchPanels.get(i).getPool());
            gc.gridy++;

        }

        topPanel.add(combobox);
        add(mainPanel, BorderLayout.CENTER);
        add(topPanel, BorderLayout.NORTH);
        setLocationRelativeTo(null);
        setSize(600, 500);
        setVisible(true);
    }

    public static void main(String[] args) {
        ShowMatchesFrame sf = new ShowMatchesFrame();
        sf.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void showMatch() {
        PresentationHandler.getInstance().getMatches((String) combobox.getSelectedItem());

        for (int i = 0; i < match.size(); i++) {
            team1.add(match.get(i).getTeams().get(0));
            team2.add(match.get(i).getTeams().get(1));
            MatchPanel m1 = new MatchPanel();
            m1.setTeam1(team1.get(i).getTeamName());
            m1.setTeam2(team2.get(i).getTeamName());
            m1.setPool(team1.get(i).getPoolName());
            matchPanels.add(m1);
        }
    }

}
