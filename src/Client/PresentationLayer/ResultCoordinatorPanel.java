package Client.PresentationLayer;

import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Nour
 */
public class ResultCoordinatorPanel extends JPanel
{
    private JLabel matchIDlbl, team1lbl, team2lbl, score1lbl, score2lbl, startlbl, endlbl;
    private JTextField matchIDtf, team1tf, team2tf, score1tf, score2tf, starttf, endtf;

    public ResultCoordinatorPanel() 
    {
        matchIDlbl = new JLabel("Match ID ");
        team1lbl = new JLabel("Team 1 ");
        team2lbl = new JLabel("Team 2 ");
        score1lbl = new JLabel("Team1 Score ");
        score2lbl = new JLabel("Team2 Score ");
        startlbl = new JLabel("Start ");
        endlbl = new JLabel("End ");
        matchIDtf = new JTextField(5);
        matchIDtf.setEditable(false);
        team1tf = new JTextField(10);
        team1tf.setEditable(false);
        team2tf = new JTextField(10);
        team2tf.setEditable(false);
        score1tf = new JTextField(4);
        score2tf = new JTextField(4);
        starttf = new JTextField(10);
        starttf.setEditable(false);
        endtf = new JTextField(10);
        endtf.setEditable(false);
        
        setLayout(new GridLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.anchor = GridBagConstraints.WEST;
        gc.gridx = 1;
        gc.gridy = 0;
        add(matchIDlbl, gc);
        gc.gridx = 2;
        gc.gridy = 0;
        add(matchIDtf, gc);
        gc.gridx = 0;
        gc.gridy = 1;
        add(startlbl, gc);
        gc.gridx = 1;
        gc.gridy = 1;
        add(starttf, gc);
        gc.gridx = 2;
        gc.gridy = 1;
        add(endlbl, gc);
        gc.gridx = 3;
        gc.gridy = 1;
        add(endtf, gc);
        gc.gridx = 0;
        gc.gridy = 2;
        add(team1lbl, gc);
        gc.gridx = 1;
        gc.gridy = 2;
        add(team1tf, gc);
        gc.gridx = 2;
        gc.gridy = 2;
        add(score1lbl, gc);
        gc.gridx = 3;
        gc.gridy = 2;
        add(score1tf, gc);
        gc.gridx = 0;
        gc.gridy = 3;
        add(team2lbl, gc);
        gc.gridx = 1;
        gc.gridy = 3;
        add(team2tf, gc);
        gc.gridx = 2;
        gc.gridy = 3;
        add(score2lbl, gc);
        gc.gridx = 3;
        gc.gridy = 3;
        add(score2tf, gc);   
    }

    public JLabel getMatchIDlbl() {
        return matchIDlbl;
    }

    public void setMatchIDlbl(JLabel matchIDlbl) {
        this.matchIDlbl = matchIDlbl;
    }

    public JLabel getTeam1lbl() {
        return team1lbl;
    }

    public void setTeam1lbl(JLabel team1lbl) {
        this.team1lbl = team1lbl;
    }

    public JLabel getTeam2lbl() {
        return team2lbl;
    }

    public void setTeam2lbl(JLabel team2lbl) {
        this.team2lbl = team2lbl;
    }

    public JLabel getScore1lbl() {
        return score1lbl;
    }

    public void setScore1lbl(JLabel score1lbl) {
        this.score1lbl = score1lbl;
    }

    public JLabel getScore2lbl() {
        return score2lbl;
    }

    public void setScore2lbl(JLabel score2lbl) {
        this.score2lbl = score2lbl;
    }

    public JTextField getMatchIDtf() {
        return matchIDtf;
    }

    public void setMatchIDtf(String matchIDtf) {
        this.matchIDtf.setText(matchIDtf);
    }

    public JTextField getTeam1tf() {
        return team1tf;
    }

    public void setTeam1tf(String team1tf) {
        this.team1tf.setText(team1tf);
    }

    public JTextField getTeam2tf() {
        return team2tf;
    }

    public void setTeam2tf(String team2tf) {
        this.team2tf.setText(team2tf);
    }

    public JTextField getScore1tf() {
        return score1tf;
    }

    public void setScore1tf(String score1tf) {
        this.score1tf.setText(score1tf);
    }

    public JTextField getScore2tf() {
        return score2tf;
    }

    public void setScore2tf(String score2tf) {
        this.score2tf.setText(score2tf);
    }

    public JLabel getStartlbl() {
        return startlbl;
    }

    public void setStartlbl(JLabel startlbl) {
        this.startlbl = startlbl;
    }

    public JLabel getEndlbl() {
        return endlbl;
    }

    public void setEndlbl(JLabel endlbl) {
        this.endlbl = endlbl;
    }

    public JTextField getStarttf() {
        return starttf;
    }

    public void setStarttf(String starttf) {
        this.starttf.setText(starttf);
    }

    public JTextField getEndtf() {
        return endtf;
    }

    public void setEndtf(String endtf) {
        this.endtf.setText(endtf);
    }
    
}
