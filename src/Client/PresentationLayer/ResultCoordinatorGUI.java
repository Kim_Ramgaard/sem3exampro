package Client.PresentationLayer;

import Client.Domain.Match;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Nour
 */
public class ResultCoordinatorGUI extends JPanel {

    private JButton addMatch, savebtn, cancelbtn;
    private JLabel matchlbl;
    private JPanel buttonPanel, mainPanel;
    private static GridBagConstraints gc = new GridBagConstraints();
    private static int gy;
    private List<ResultCoordinatorPanel> matches = new ArrayList<>();
    private ButtonHandler bh = new ButtonHandler();

    public ResultCoordinatorGUI() {
        setLayout(new BorderLayout());
        GridBagConstraints gc = new GridBagConstraints();
        savebtn = new JButton("Save");
        cancelbtn = new JButton("Cancel");
        buttonPanel = new JPanel();
        addMatch = new JButton("+Add match");
        matchlbl = new JLabel("Matches");;
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        
        gc.anchor = GridBagConstraints.WEST;
        gc.gridx = 0;
        mainPanel.add(matchlbl, gc);
        gc.insets = new Insets(0, 0, 5, 0);

        
        for (int i = 0; i < 4; i++) {
            matches.add(new ResultCoordinatorPanel());
        }

        for (int i = 0; i < matches.size(); i++) {
            gc.gridwidth = 6;
            gc.gridy++;
            gc.gridx = 0;
            mainPanel.add(matches.get(i), gc);
        }
        gy = gc.gridy;
        gy++;

        buttonPanel.add(addMatch);
        buttonPanel.add(savebtn);
        buttonPanel.add(cancelbtn);
        addMatch.addActionListener(bh);
        savebtn.addActionListener(bh);
        cancelbtn.addActionListener(bh);

        add(buttonPanel, BorderLayout.SOUTH);
        add(mainPanel, BorderLayout.CENTER);
    }
    
    public class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addMatch) {
                gc.gridy = gy++;
//                addPlayer();
                revalidate();
            } else if (e.getSource() == savebtn) {
//                registerTeam();               

            } else if (e.getSource() == cancelbtn) {
//                SignUpGUIHolder.swapView("Info");
            }

        }
    }
    
    //add matches to the GUI 
    public void addMatch() {
        matches.add(new ResultCoordinatorPanel());
        gc.insets = new Insets(0, 0, 5, 0);
        gc.gridwidth = 6;
        gc.gridx = 0;
        mainPanel.add(matches.get(matches.size() - 1), gc);
        gc.gridwidth = 1;
        gc.gridx = 7;
        revalidate();
    }
    
    //this will register the match scores to the database 
    public void registerMatch() {
        Match[] matchDomain = new Match[matches.size()];
        for (int i = 0; i < matchDomain.length; i++) {
//            Player p = new Player(players.get(i).getFirstNameTF(), players.get(i).getLastNameTF(), players.get(i).getSchoolClassTF());
//            playerDomain[i] = p;
        }
//        TeamManager tm = new TeamManager(managerFNameTF.getText(), managerLNameTF.getText(), phoneNumberTF.getText(), emailTF.getText());
//        Team team = new Team(teamNameTF.getText(), playerDomain, tm);
//        SignUp signUp = new SignUp(team);
//        PresentationHandler.getInstance().storeSignup(signUp);
//        //PresentationHandler.getInstance().storeTournament(t);
//        JFrame frame = new JFrame();
//        //pop up dialog 
//        JOptionPane.showMessageDialog(frame, "Your team has now been added to the tournament as: "+teamNameTF.getText());
//        //switch back to previous panel
//        SignUpGUIHolder.swapView("Info");
    }   
}