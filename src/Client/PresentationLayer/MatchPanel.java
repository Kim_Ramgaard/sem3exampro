/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client.PresentationLayer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.*;

/**
 *
 * @author Niels
 */
public class MatchPanel extends JPanel {

    private JLabel team1, team2, score1, score2, pool;
  
    public MatchPanel() {
        
        team1 = new  JLabel(); 
        team2 = new JLabel();
        score1 = new JLabel();
        score2 = new JLabel();
        pool = new JLabel();                 
        
        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints(); 
        
        gc.insets = new Insets(0,0,10,10); 
        gc.gridx =0; 
        add(team1,gc);
        gc.gridx++; 
        add(score1);
        gc.gridx++; 
        add(score2); 
        gc.gridx++;
        add(team2);
        gc.gridx++;
        add(pool);        
        
    }

    public JLabel getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1.setText(team1);
    }

    public JLabel getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2.setText(team2);
    }

    public JLabel getSore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1.setText(score1);
    }

    public JLabel getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2.setText(score2);
    }

    public JLabel getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool.setText(pool);
    }

}
