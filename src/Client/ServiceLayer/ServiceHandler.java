
package Client.ServiceLayer;

/**
 *
 * @author Kim Ramgaard
 */
public class ServiceHandler {
    
    private ServiceHandler() {
    }
    
    public static ServiceHandler getInstance() {
        return ServiceHandlerHolder.INSTANCE;
    }
    
    private static class ServiceHandlerHolder {

        private static final ServiceHandler INSTANCE = new ServiceHandler();
    }
}
